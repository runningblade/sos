#ifndef SDPA_GMP_H
#define SDPA_GMP_H

#if defined(SDPA_GMP_SUPPORT)||defined(SDPA_DD_SUPPORT)||defined(SDPA_QD_SUPPORT)
#include <sdpa_parts.h>
#include <CommonFile/MathBasic.h>

PRJ_BEGIN

class SDPAGMP
{
  struct Entry {
    Entry();
    Entry(int bid,int row,int col,double val);
    bool operator<(const Entry& other) const;
    int _bid,_row,_col;
    double _val;
  };
public:
#ifdef SDPA_GMP_SUPPORT
  typedef mpf_class SDPA_FLOAT;
#endif
#ifdef SDPA_DD_SUPPORT
  typedef dd_real SDPA_FLOAT;
#endif
#ifdef SDPA_QD_SUPPORT
  typedef qd_real SDPA_FLOAT;
#endif
  typedef Eigen::Matrix<SDPA_FLOAT,-1,1> Vec;
  enum ParameterType {PARAMETER_DEFAULT,PARAMETER_UNSTABLE_BUT_FAST,PARAMETER_STABLE_BUT_SLOW};
  enum phaseType {noINFO,pFEAS,dFEAS,pdFEAS,pdINF,pFEAS_dINF,pINF_dFEAS,pdOPT,pUNBD,dUNBD};
  enum ConeType {SDP,SOCP,LP};

  SDPAGMP();
  void setParameterType(ParameterType type=PARAMETER_DEFAULT);
  void setParameterMaxIteration(int maxIteration);
  void initializeUpperTriangleSpace();
  void initializeUpperTriangle();
  void initializeSolve();
  void fillSDPCone();
  void fillLPCone();

  void solve(sdpa::Solutions* ptInit=NULL);
  void setDisplay(FILE* Display=stdout);
  phaseType getPhaseValue() const;

  void inputConstraintNumber(int m);
  void inputBlockNumber(int nBlock);
  void inputBlockSize(int l,int size);
  void inputBlockType(int l,ConeType coneType);
  void inputCVec(int k,double value);
  void inputElement(int k,int l,int i,int j,double value,bool inputCheck=true);

  const SDPA_FLOAT* getResultXVec() const;
  double get_d(const SDPA_FLOAT& f) const;
  void reorderCones();
  int nSDPCone() const;
  int nLPCone() const;
  int nBlock() const;
  int m() const;
private:
  sdpa::Parameter _param;
  sdpa::InputData _inputData;
  sdpa::ComputeTime _com;
  sdpa::Solutions _currentPt;
  sdpa::SolveInfo::phaseType _retCode;
  //raw data
  std::vector<int> _blockStruct,_blockStructSDP,_reorderIndex;
  std::vector<std::vector<Entry>> _varSDP,_varLP;
  //display
  FILE* _Display;
};

PRJ_END
#endif

#endif
