#ifndef SOS_GRAPH_H
#define SOS_GRAPH_H

#include "SparseUtils.h"
#include "SemiAlgebraicSet.h"

PRJ_BEGIN

class SOSGraph
{
  typedef typename ScalarUtil<scalarD>::ScalarCol Vec;
  typedef Eigen::SparseMatrix<scalarD,0,sizeType> SMat;
  typedef typename ScalarUtil<scalarD>::ScalarMat DMat;
  typedef Eigen::Triplet<scalarD,sizeType> STrip;
  typedef ParallelVector<STrip> STrips;
  typedef SOSPolynomial<scalarD,'x'> PolyX;
  typedef SOSPolynomial<PolyX,'a'> PolyXA;
public:
  enum ORDER
  {
    METIS,
    NATURAL,
    METIS_REVERSED,
    NONE,
  };
  SOSGraph(const PolyXA& poly,const SemiAlgebraicSet* K,ORDER type=METIS_REVERSED);
  SOSGraph(const Matd& csp,ORDER type=METIS_REVERSED);
  bool checkRIPMaximalCliques() const;
  sizeType getCliqueIdFirst(const PolyXA& poly) const;
  std::vector<sizeType> getCliqueIdAll(const PolyXA& poly) const;
  const std::set<sizeType>& getClique(const PolyXA& poly) const;
  const std::vector<std::set<sizeType>>& getMaximalCliques() const;
  void writeVTK(const std::string& csp,const std::string& cspE) const;
protected:
  SMat toSparse(const Matd& p) const;
  std::vector<sizeType> fillReducing() const;
  Matd extend(const std::vector<sizeType>& order,std::vector<std::set<sizeType>>& cliques) const;
  void addF2Csp(Matd& csp,const PolyXA& poly) const;
  void compute(const PolyXA& poly,const SemiAlgebraicSet* K,ORDER type);
  void compute(const Matd& csp,ORDER type);
  //data
  std::vector<std::set<sizeType>> _cliques;
  Matd _csp,_cspE;
};

PRJ_END

#endif
