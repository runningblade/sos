#ifndef SOS_PROBLEM_H
#define SOS_PROBLEM_H

#include "SOSGraph.h"
#include "SMatNumber.h"
#include "SOSPolynomial.h"
#include <tinyxml2.h>
#ifdef USE_MOSEK_9
#include <mosek.h>
#endif
#ifdef SCS_SUPPORT
#include "ScsInterface.h"
#endif
#ifdef SDPA_SUPPORT
#include <sdpa_call.h>
#endif
#if defined(SDPA_GMP_SUPPORT)||defined(SDPA_DD_SUPPORT)||defined(SDPA_QD_SUPPORT)
#include "SDPAGMP.h"
#endif

PRJ_BEGIN

struct SOSProblem;
struct SOSVariable
{
  SOSVariable();
  SOSVariable(sizeType id,scalarD objCoef);
  operator sizeType() const;
  sizeType _id;
  scalarD _objCoef;
};
struct SOSConstraint
{
  typedef typename ScalarUtil<scalarD>::ScalarCol Vec;
  typedef Eigen::SparseMatrix<scalarD,0,sizeType> SMat;
  typedef typename ScalarUtil<scalarD>::ScalarMat DMat;
  typedef Eigen::Triplet<scalarD,sizeType> STrip;
  typedef ParallelVector<STrip> STrips;
  typedef std::unordered_map<std::string,SOSVariable> VARMAP;
  typedef std::unordered_map<sizeType,std::string> VARMAP_INV;
  typedef std::unordered_map<sizeType,scalarD> VARMAP_VALUE;
  typedef std::vector<sizeType> VARREMAP;
  virtual ~SOSConstraint();
};
struct PolynomialConstraint : public SOSConstraint
{
private:
  typedef SOSPolynomial<scalarD,'x'> PolyX;
  typedef SOSTerm<scalarD,'x'> TermX;
  friend struct SOSProblem;
public:
  PolynomialConstraint();
  PolynomialConstraint(scalarD b,bool eq);
  PolynomialConstraint(SOSProblem& prob,const std::string& name,const PolyX& p,bool eq);
#ifdef USE_MOSEK_9
  bool setToMosek(MSKint32t& cid,MSKenv_t env,MSKtask_t task) const;
#endif
#ifdef SCS_SUPPORT
  bool setToScs(ScsInterface& scs,sizeType nrVar) const;
#endif
#ifdef SDPA_SUPPORT
  bool setToSdpa(SDPA& sdpa,sizeType& cid,bool prepare) const;
#endif
#if defined(SDPA_GMP_SUPPORT)||defined(SDPA_DD_SUPPORT)||defined(SDPA_QD_SUPPORT)
  bool setToSdpaGmp(SDPAGMP& sdpa,sizeType& cid,bool prepare) const;
#endif
  PolynomialConstraint eliminate(const VARMAP_VALUE& vars) const;
  std::string toString(const VARMAP_INV& varsInv) const;
  void remap(const VARREMAP& varRemap);
  scalarD eval(const Vec& v) const;
  bool isDummy(scalarD eps) const;
  bool eq() const;
protected:
  PolyX& LHS();
  //data
  PolyX _LHS;
  bool _eq;
};
struct PMIConstraint : public SOSConstraint
{
private:
  typedef SOSPolynomial<SMatNumber<scalarD>,'x'> PolyX;
  typedef SOSTerm<SMatNumber<scalarD>,'x'> TermX;
  typedef SOSPolynomial<scalarD,'x'> PolyXS;
  typedef SOSTerm<scalarD,'x'> TermXS;
  friend struct SOSProblem;
public:
  PMIConstraint();
  PMIConstraint(const SMat& b);
  PMIConstraint(SOSProblem& prob,const std::string& name,const PolyX& p);
#ifdef USE_MOSEK_9
  bool setToMosek(MSKint32t pmiId,MSKint32t& cid,MSKenv_t env,MSKtask_t task) const;
#endif
#ifdef SCS_SUPPORT
  bool setToScs(ScsInterface& scs,sizeType nrVar) const;
#endif
#ifdef SDPA_SUPPORT
  bool setToSdpa(SDPA& sdpa,sizeType& cid,bool prepare) const;
#endif
#if defined(SDPA_GMP_SUPPORT)||defined(SDPA_DD_SUPPORT)||defined(SDPA_QD_SUPPORT)
  bool setToSdpaGmp(SDPAGMP& sdpa,sizeType& cid,bool prepare) const;
#endif
  PMIConstraint eliminate(const VARMAP_VALUE& vars) const;
  std::string toString(const VARMAP_INV& varsInv) const;
  void remap(const VARREMAP& varRemap);
  SMat eval(const Vec& v) const;
  bool isDummy(scalarD eps) const;
  sizeType nCons() const;
  sizeType n() const;
protected:
  PolyX& LHS();
  //data
  PolyX _LHS;
};
struct SOSProblem
{
  typedef typename ScalarUtil<scalarD>::ScalarCol Vec;
  typedef Eigen::SparseMatrix<scalarD,0,sizeType> SMat;
  typedef typename ScalarUtil<scalarD>::ScalarMat DMat;
  typedef Eigen::Triplet<scalarD,sizeType> STrip;
  typedef ParallelVector<STrip> STrips;
  typedef SOSPolynomial<scalarD,'x'> PolyX;
  typedef SOSPolynomial<scalarD,'a'> PolyA;
  typedef SOSPolynomial<PolyX,'a'> PolyXA;
  typedef SOSTerm<scalarD,'x'> TermX;
  typedef SOSTerm<scalarD,'a'> TermA;
  typedef SOSTerm<PolyX,'a'> TermXA;
  typedef SOSConstraint::VARMAP VARMAP;
  typedef SOSConstraint::VARMAP_INV VARMAP_INV;
  typedef SOSConstraint::VARMAP_VALUE VARMAP_VALUE;
  typedef std::map<std::string,std::shared_ptr<PolynomialConstraint>> POLYS;
  typedef std::map<std::string,std::shared_ptr<PMIConstraint>> PMIS;
  friend struct PolynomialConstraint;
  friend struct PMIConstraint;
public:
  enum VARIABLE_OP
  {
    MUST_NEW,
    MUST_EXIST,
    NEW_OR_EXIST,
  };
  enum SOLVER_TYPE
  {
    SOLVER_MOSEK,
    SOLVER_SCS,
    SOLVER_SDPA,
    SOLVER_SDPA_GMP,
  };
  enum STATUS_TYPE
  {
    STATUS_UNBOUNDED,
    STATUS_INFEASIBLE,
    STATUS_OPTIMAL,
    STATUS_ERROR,
  };
  SOSProblem();
  SOSProblem(const SOSProblem& other);
  SOSProblem& operator=(const SOSProblem& other);
  PolyXA fullPoly(sizeType nrVar,const std::set<sizeType>* clique,sizeType d) const;
  void makeBSOS(const std::string& nameCons,const std::string& namePoly,const PolyXA& poly,const SemiAlgebraicSet& K,sizeType d,sizeType k);    //BSOS is different from PutSOS in constrained cases, otherwise just use PutSOS
  void makePutSOS(const std::string& nameCons,const std::string& namePoly,const PolyXA& poly,const SemiAlgebraicSet* K,sizeType d);
  void makePutSOSConvex(const std::string& nameCons,const std::string& namePoly,const PolyXA& poly,sizeType d);
  void makePointInside(const std::string& nameCons,const std::string& namePoly,const PolyXA& poly,const Vec& point,scalarD level);
  void makeRelaxedVolume(const std::string& nameCons,const std::string& nameConsSOS,const std::string& namePoly);
  void makeConstraint(const std::string& nameCons,const std::string& namePoly,const PolyX& poly,bool eq=true);
  void makeObjective(const std::string& namePoly,const PolyX& poly);
  void removeDummyConstraints(scalarD eps);
  //property
  void maxIterations(sizeType maxIter);
  sizeType maxIterations() const;
  void usePolyMonomial(bool use);
  bool usePolyMonomial() const;
  void useGraphOrder(SOSGraph::ORDER type);
  SOSGraph::ORDER useGraphOrder() const;
  void checkConsistency(bool check);
  bool checkConsistency() const;
  void visualizeCSP(const std::string& path);
  std::string visualizeCSP() const;
  void writeFilePath(const std::string& path);
  std::string writeFilePath() const;
  void outputWhenSolve(bool output);
  bool outputWhenSolve() const;
  void solverType(SOLVER_TYPE output);
  SOLVER_TYPE solverType() const;
  void solverStatus(STATUS_TYPE type);
  STATUS_TYPE solverStatus() const;
  void useMultiThreads(bool use);
  bool useMultiThreads() const;
  //print problem
  operator std::string() const;
  void writeProb(const std::string& path) const;
  //fix a set of variables and get a new problem
  SOSProblem fix(const std::string& namePoly,const VARMAP_VALUE& vals) const;    //this version eliminate all variables
  //solve
  sizeType nrPoly() const;
  sizeType nrPMI() const;
  const PolynomialConstraint& poly(sizeType i) const;
  const PMIConstraint& PMI(sizeType i) const;
  PolyA evalX(const std::string& name,const PolyXA& poly) const;
  const Vec& solution() const;
  bool solve();
  //debug
  static void debugMonomials(sizeType nrVar,sizeType nrDeg);
  static void debugTerms(const PolyXA& poly);
  //get variable
  scalarD getSolutionOfPolyVar(const std::string& name,sizeType id) const;
  scalarD getSolutionOfVar(const std::string& nameVar) const;
protected:
  static void makeUnique(std::vector<TermXA>& t);
  std::vector<SOSProblem::TermXA> buildMonomial(const PolyXA& poly,const std::set<sizeType>* clique,sizeType d) const;
  void buildMonomial(std::vector<TermXA>& ret,const TermXA& tPoly,const TermXA& t,sizeType currVar,sizeType d) const;
  std::vector<SOSProblem::TermXA> buildMonomial(sizeType nrVarY,const std::set<sizeType>* clique,sizeType d) const;
  void buildMonomial(std::vector<TermXA>& ret,const TermXA& t,sizeType currVar,sizeType nrVar,sizeType d) const;
  //Problem based on Krivine-Stengle-Putinar's Positivstellensatz (bounded dgree SOS)
  void makeBSOSOnK(const std::string& nameCons,const std::string& namePoly,const PolyXA& poly,const SemiAlgebraicSet& K,const SOSGraph* g,sizeType d,sizeType k);
  void makeDenseBSOS(const std::string& nameCons,const std::string& namePoly,PolyXA poly,const SemiAlgebraicSet& K,sizeType d,sizeType k);
  void makeSparseBSOS(const std::string& nameCons,const std::string& namePoly,PolyXA poly,const SemiAlgebraicSet& K,const SOSGraph& g,sizeType d,sizeType k);
  void buildLagrangianBSOS(const std::string& nameCons,const std::string& namePoly,PolyXA& polyL,const SemiAlgebraicSet& K,const SOSGraph* g,sizeType d);
  void buildLagrangianBSOS(const std::string& nameCons,const std::string& namePoly,std::vector<PolyXA>& Lags,PolyXA p,const SemiAlgebraicSet& K,sizeType& nrVarX,sizeType currF,sizeType d,bool mustPositive);
  //Problem based on Putinar's Positivstellensatz
  void makePutSOSOnK(const std::string& nameCons,const std::string& namePoly,const PolyXA& poly,const SemiAlgebraicSet& K,const SOSGraph* g,sizeType d);
  void makeDensePutSOS(const std::string& nameCons,const std::string& namePoly,const PolyXA& poly,const std::set<sizeType>* clique,sizeType d);
  void makeSparsePutSOS(const std::string& nameCons,const std::string& namePoly,const PolyXA& poly,const SOSGraph& g,sizeType d);
  //decl. variable
  std::string SOSVarName(const std::string& name,sizeType r) const;
  std::string SOSVarName(const std::string& name,sizeType r,sizeType c) const;
  std::string relaxedVolumeVarName(const std::string& name,sizeType r,sizeType c) const;
  std::string polyVarName(const std::string& name,sizeType id) const;
  SOSVariable& SOSVar(const std::string& name,sizeType r,VARIABLE_OP op=NEW_OR_EXIST);
  SOSVariable& SOSVar(const std::string& name,sizeType r,sizeType c,VARIABLE_OP op=NEW_OR_EXIST);
  SOSVariable& relaxedVolumeVar(const std::string& name,sizeType r,sizeType c,VARIABLE_OP op=NEW_OR_EXIST);
  SOSVariable& polyVar(const std::string& name,sizeType id,VARIABLE_OP op=NEW_OR_EXIST);
  SOSVariable& var(const std::string& nameVar,VARIABLE_OP op=NEW_OR_EXIST);
  //add constraint
  void addPolynomialConstraint(const std::string& nameCons,std::shared_ptr<PolynomialConstraint> c);
  void addPMIConstraint(const std::string& nameCons,std::shared_ptr<PMIConstraint> c);
  SMat EIJSym(sizeType n,sizeType r,sizeType c) const;
  //solve
#ifdef USE_MOSEK_9
  bool solveMosek();
#endif
#ifdef SCS_SUPPORT
  bool solveScs();
#endif
#ifdef SDPA_SUPPORT
  bool solveSdpa();
#endif
#if defined(SDPA_GMP_SUPPORT)||defined(SDPA_DD_SUPPORT)||defined(SDPA_QD_SUPPORT)
  bool solveSdpaGmp();
#endif
  //data
  tinyxml2::XMLDocument _pt;
  std::shared_ptr<Vec> _solution;
  //std::vector<DMat> _pmiMosek;
  VARMAP _vars;
  POLYS _poly;
  PMIS _pmi;
};

PRJ_END

#endif
