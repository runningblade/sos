#include "SOSProblem.h"
#include "Utils.h"
#ifdef SCS_SUPPORT
#include <glbopts.h>
#endif

USE_PRJ_NAMESPACE

#define SAFE_CALL(CALL) {   \
r=CALL;  \
if(r!=MSK_RES_OK) {   \
  INFOV("Mosek returns error code: %ld, line: %ld!",(sizeType)r,(sizeType)__LINE__)  \
  if(task)  \
    MSK_deleteenv(&env);    \
  if(env)   \
    MSK_deletetask(&task);  \
  return false; \
}   \
}
//SOSVariable
SOSVariable::SOSVariable():_id(-1),_objCoef(0) {}
SOSVariable::SOSVariable(sizeType id,scalarD objCoef):_id(id),_objCoef(objCoef) {}
SOSVariable::operator sizeType() const
{
  return _id;
}
//SOSConstraint
SOSConstraint::~SOSConstraint() {}
//PolynomialConstraint
PolynomialConstraint::PolynomialConstraint() {}
PolynomialConstraint::PolynomialConstraint(scalarD b,bool eq):_LHS(ScalarOfT<PolyX>::convert(b)),_eq(eq) {}
PolynomialConstraint::PolynomialConstraint(SOSProblem& prob,const std::string& name,const PolyX& p,bool eq):_LHS(0),_eq(eq)
{
  std::vector<sizeType> ids(p.nrVar());
  for(sizeType i=0; i<(sizeType)ids.size(); i++)
    ids[i]=prob.polyVar(name,i);
  _LHS=p.rename(ids);
}
#ifdef USE_MOSEK_9
bool PolynomialConstraint::setToMosek(MSKint32t& cid,MSKenv_t env,MSKtask_t task) const
{
  MSKrescodee r;
  if(_LHS.order()>1) {
    ASSERT_MSG(false,"Cannot create higher order polynomial constraint in Mosek!")
  } else {
    scalarD b=0;
    std::vector<MSKint32t> cols;
    std::vector<MSKrealt> coefs;
    for(sizeType i=0; i<(sizeType)_LHS._terms.size(); i++)
      if(_LHS._terms[i].order()==0)
        b-=_LHS._terms[i]._coef;
      else {
        cols.push_back(_LHS._terms[i]._id[0]);
        coefs.push_back(_LHS._terms[i]._coef);
      }
    SAFE_CALL(MSK_putarow(task,cid,cols.size(),&cols[0],&coefs[0]))
    SAFE_CALL(MSK_putconbound(task,cid,_eq?MSK_BK_FX:MSK_BK_LO,b,b))
    cid++;
  }
  return true;
}
#endif
#ifdef SCS_SUPPORT
bool PolynomialConstraint::setToScs(ScsInterface& scs,sizeType nrVar) const
{
  if(_LHS.order()>1) {
    ASSERT_MSG(false,"Cannot create higher order polynomial constraint in Scs!")
  } else {
    Vec b=Vec::Constant(1,0);
    STrips trips;
    for(sizeType i=0; i<(sizeType)_LHS._terms.size(); i++)
      if(_LHS._terms[i].order()==0)
        b[0]+=_LHS._terms[i]._coef;
      else trips.push_back(STrip(0,_LHS._terms[i]._id[0],-_LHS._terms[i]._coef)); //Scs assumes b-A*x
    SMat A;
    A.resize(1,nrVar);
    A.setFromTriplets(trips.begin(),trips.end());
    scs.constraint(A,b,_eq?ScsInterface::EQUALITY:ScsInterface::INEQUALITY);
  }
  return true;
}
#endif
#ifdef SDPA_SUPPORT
bool PolynomialConstraint::setToSdpa(SDPA& sdpa,sizeType& cid,bool prepare) const
{
  if(_LHS.order()>1) {
    ASSERT_MSG(false,"Cannot create higher order polynomial constraint in Sdpa!")
  } else {
    if(prepare) {
      sdpa.inputBlockSize(cid,_eq?2:1);
      sdpa.inputBlockType(cid,SDPA::LP);
    } else {
      scalarD coef=1;
      for(sizeType pass=1,nrPass=_eq?2:1; pass<=nrPass; pass++,coef*=-1)
        for(sizeType i=0; i<(sizeType)_LHS._terms.size(); i++)
          if(_LHS._terms[i].order()==0)
            sdpa.inputElement(0,cid,pass,pass,-_LHS._terms[i]._coef*coef);
          else sdpa.inputElement(_LHS._terms[i]._id[0]+1,cid,pass,pass,_LHS._terms[i]._coef*coef);
    }
    cid++;
  }
  return true;
}
#endif
#if defined(SDPA_GMP_SUPPORT)||defined(SDPA_DD_SUPPORT)||defined(SDPA_QD_SUPPORT)
bool PolynomialConstraint::setToSdpaGmp(SDPAGMP& sdpa,sizeType& cid,bool prepare) const
{
  if(_LHS.order()>1) {
    ASSERT_MSG(false,"Cannot create higher order polynomial constraint in Sdpa!")
  } else {
    if(prepare) {
      for(sizeType pass=0,nrPass=_eq?2:1; pass<nrPass; pass++,cid++) {
        sdpa.inputBlockSize(cid,1);
        sdpa.inputBlockType(cid,SDPAGMP::LP);
      }
    } else {
      scalarD coef=1;
      for(sizeType pass=0,nrPass=_eq?2:1; pass<nrPass; pass++,cid++,coef*=-1)
        for(sizeType i=0; i<(sizeType)_LHS._terms.size(); i++)
          if(_LHS._terms[i].order()==0)
            sdpa.inputElement(0,cid,1,1,-_LHS._terms[i]._coef*coef);
          else sdpa.inputElement(_LHS._terms[i]._id[0]+1,cid,1,1,_LHS._terms[i]._coef*coef);
    }
  }
  return true;
}
#endif
PolynomialConstraint PolynomialConstraint::eliminate(const VARMAP_VALUE& vars) const
{
  PolynomialConstraint ret;
  ret._LHS=_LHS.linearTransform(vars);
  ret._eq=_eq;
  return ret;
}
std::string PolynomialConstraint::toString(const VARMAP_INV& varsInv) const
{
  std::ostringstream oss;
  oss << _LHS.toString(&varsInv);
  if(_eq)
    oss << "=0";
  else oss << ">=0";
  oss << std::endl;
  return oss.str();
}
void PolynomialConstraint::remap(const VARREMAP& varRemap)
{
  _LHS=_LHS.varRemap(varRemap);
}
scalarD PolynomialConstraint::eval(const Vec& v) const
{
  return _LHS.eval(v);
}
bool PolynomialConstraint::isDummy(scalarD eps) const
{
  for(sizeType i=0; i<(sizeType)_LHS._terms.size(); i++)
    if(!_LHS._terms[i]._id.empty() && std::abs(_LHS._terms[i]._coef)>eps)
      return false;
  return true;
}
bool PolynomialConstraint::eq() const
{
  return _eq;
}
PolynomialConstraint::PolyX& PolynomialConstraint::LHS()
{
  return _LHS;
}
//PMIConstraint
PMIConstraint::PMIConstraint() {}
PMIConstraint::PMIConstraint(const SMat& b):_LHS(ScalarOfT<PolyX>::convert(b)) {}
PMIConstraint::PMIConstraint(SOSProblem& prob,const std::string& name,const PolyX& p)
{
  std::vector<sizeType> ids(p.nrVar());
  for(sizeType i=0; i<(sizeType)ids.size(); i++)
    ids[i]=prob.polyVar(name,i);
  _LHS=p.rename(ids);
}
#ifdef USE_MOSEK_9
bool PMIConstraint::setToMosek(MSKint32t pmiId,MSKint32t& cid,MSKenv_t env,MSKtask_t task) const
{
  MSKrescodee r;
  sizeType N=n();
  sizeType NCons=nCons();
  if(_LHS.order()>1) {
    ASSERT_MSG(false,"Cannot create higher order polynomial constraint in Mosek!")
  }
  std::vector<scalarD> bs(NCons,0);
  std::vector<std::vector<MSKint32t>> cols(NCons);
  std::vector<std::vector<MSKrealt>> coefs(NCons);
  for(sizeType i=0; i<(sizeType)_LHS._terms.size(); i++) {
    ASSERT(_LHS._terms[i]._coef.getSMat().rows()==N)
    ASSERT(_LHS._terms[i]._coef.getSMat().cols()==N)
    if(_LHS._terms[i].order()==0) {
      for(sizeType c=0; c<N; c++)
        for(SMat::InnerIterator it(_LHS._terms[i]._coef.getSMat(),c); it; ++it)
          if(it.row()<=it.col())
            bs[IJToTri(N,it.row(),it.col())]-=it.value();
    } else {
      for(sizeType c=0; c<N; c++)
        for(SMat::InnerIterator it(_LHS._terms[i]._coef.getSMat(),c); it; ++it)
          if(it.row()<=it.col()) {
            cols[IJToTri(N,it.row(),it.col())].push_back(_LHS._terms[i]._id[0]);
            coefs[IJToTri(N,it.row(),it.col())].push_back(it.value());
          }
    }
  }
  MSKrealt weight=1;
  MSKint64t handle;
  sizeType row,col;
  for(sizeType i=0; i<(sizeType)bs.size(); i++,cid++) {
    TriToIJ(N,i,row,col);
    std::swap(row,col);
    MSKrealt val=row==col?-1:-0.5;
    MSKint32t rowMSK=row,colMSK=col;
    SAFE_CALL(MSK_appendsparsesymmat(task,N,1,&rowMSK,&colMSK,&val,&handle))
    SAFE_CALL(MSK_putbaraij(task,cid,pmiId,1,&handle,&weight))
    SAFE_CALL(MSK_putarow(task,cid,cols[i].size(),&(cols[i][0]),&(coefs[i][0])))
    SAFE_CALL(MSK_putconbound(task,cid,MSK_BK_FX,bs[i],bs[i]))
  }
  return true;
}
#endif
#ifdef SCS_SUPPORT
bool PMIConstraint::setToScs(ScsInterface& scs,sizeType nrVar) const
{
  sizeType N=n();
  sizeType NCons=nCons();
  if(_LHS.order()>1) {
    ASSERT_MSG(false,"Cannot create higher order polynomial constraint in Mosek!")
  }
  STrips trips;
  Vec b=Vec::Zero(NCons);
  scalarD sqrt2=std::sqrt(2.0);
  for(sizeType i=0; i<(sizeType)_LHS._terms.size(); i++) {
    ASSERT(_LHS._terms[i]._coef.getSMat().rows()==N)
    ASSERT(_LHS._terms[i]._coef.getSMat().cols()==N)
    if(_LHS._terms[i].order()==0) {
      for(sizeType c=0; c<N; c++)
        for(SMat::InnerIterator it(_LHS._terms[i]._coef.getSMat(),c); it; ++it)
          if(it.row()<=it.col())
            b[IJToTri(N,it.row(),it.col())]+=it.value();
    } else {
      for(sizeType c=0; c<N; c++)
        for(SMat::InnerIterator it(_LHS._terms[i]._coef.getSMat(),c); it; ++it)
          if(it.row()<=it.col())
            trips.push_back(STrip(IJToTri(N,it.row(),it.col()),_LHS._terms[i]._id[0],-it.value()*(it.row()<it.col()?sqrt2:1)));
    }
  }
  SMat A;
  A.resize(NCons,nrVar);
  A.setFromTriplets(trips.begin(),trips.end());
  scs.constraint(A,b,ScsInterface::SEMI_DEFINITE);
  return true;
}
#endif
#ifdef SDPA_SUPPORT
bool PMIConstraint::setToSdpa(SDPA& sdpa,sizeType& cid,bool prepare) const
{
  if(_LHS.order()>1) {
    ASSERT_MSG(false,"Cannot create higher order polynomial constraint in Mosek!")
  }
  sizeType N=n();
  if(prepare) {
    sdpa.inputBlockSize(cid,N);
    sdpa.inputBlockType(cid,SDPA::SDP);
  } else {
    for(sizeType i=0; i<(sizeType)_LHS._terms.size(); i++) {
      ASSERT(_LHS._terms[i]._coef.getSMat().rows()==N)
      ASSERT(_LHS._terms[i]._coef.getSMat().cols()==N)
      if(_LHS._terms[i].order()==0) {
        for(sizeType c=0; c<N; c++)
          for(typename SMat::InnerIterator it(_LHS._terms[i]._coef.getSMat(),c); it; ++it)
            if(it.row()<=it.col())
              sdpa.inputElement(0,cid,it.row()+1,it.col()+1,-it.value());
      } else {
        for(sizeType c=0; c<N; c++)
          for(typename SMat::InnerIterator it(_LHS._terms[i]._coef.getSMat(),c); it; ++it)
            if(it.row()<=it.col())
              sdpa.inputElement(_LHS._terms[i]._id[0]+1,cid,it.row()+1,it.col()+1,it.value());
      }
    }
  }
  cid++;
  return true;
}
#endif
#if defined(SDPA_GMP_SUPPORT)||defined(SDPA_DD_SUPPORT)||defined(SDPA_QD_SUPPORT)
bool PMIConstraint::setToSdpaGmp(SDPAGMP& sdpa,sizeType& cid,bool prepare) const
{
  if(_LHS.order()>1) {
    ASSERT_MSG(false,"Cannot create higher order polynomial constraint in Mosek!")
  }
  sizeType N=n();
  if(prepare) {
    sdpa.inputBlockSize(cid,N);
    sdpa.inputBlockType(cid,SDPAGMP::SDP);
  } else {
    for(sizeType i=0; i<(sizeType)_LHS._terms.size(); i++) {
      ASSERT(_LHS._terms[i]._coef.getSMat().rows()==N)
      ASSERT(_LHS._terms[i]._coef.getSMat().cols()==N)
      if(_LHS._terms[i].order()==0) {
        for(sizeType c=0; c<N; c++)
          for(typename SMat::InnerIterator it(_LHS._terms[i]._coef.getSMat(),c); it; ++it)
            if(it.row()<=it.col())
              sdpa.inputElement(0,cid,it.row()+1,it.col()+1,-it.value());
      } else {
        for(sizeType c=0; c<N; c++)
          for(typename SMat::InnerIterator it(_LHS._terms[i]._coef.getSMat(),c); it; ++it)
            if(it.row()<=it.col())
              sdpa.inputElement(_LHS._terms[i]._id[0]+1,cid,it.row()+1,it.col()+1,it.value());
      }
    }
  }
  cid++;
  return true;
}
#endif
PMIConstraint PMIConstraint::eliminate(const VARMAP_VALUE& vars) const
{
  PMIConstraint ret;
  std::unordered_map<sizeType,SMatNumber<scalarD>> varsSMat;
  for(const VARMAP_VALUE::value_type &v:vars) {
    varsSMat[v.first]=v.second;
  }
  ret._LHS=_LHS.linearTransform(varsSMat);
  return ret;
}
std::string PMIConstraint::toString(const VARMAP_INV& varsInv) const
{
  sizeType rows=_LHS._terms[0]._coef.getSMat().rows();
  sizeType cols=_LHS._terms[0]._coef.getSMat().cols();
  //fill polys
  std::vector<std::vector<PolyXS>> polys(rows,std::vector<PolyXS>(cols));
  for(sizeType i=0; i<(sizeType)_LHS._terms.size(); i++) {
    const TermX& t=_LHS._terms[i];
    for(sizeType c=0; c<t._coef.getSMat().cols(); c++)
      for(SMat::InnerIterator it(t._coef.getSMat(),c); it; ++it) {
        TermXS ts;
        ts._coef=it.value();
        ts._id=t._id;
        ts._order=t._order;
        polys[it.row()][it.col()]+=ts;
      }
  }
  //format matrix
  std::vector<std::vector<std::string>> strs(rows,std::vector<std::string>(cols));
  for(sizeType r=0; r<rows; r++)
    for(sizeType c=0; c<cols; c++)
      strs[r][c]=polys[r][c].toString(&varsInv);
  return printTable(strs);
}
void PMIConstraint::remap(const VARREMAP& varRemap)
{
  _LHS=_LHS.varRemap(varRemap);
}
PMIConstraint::SMat PMIConstraint::eval(const Vec& v) const
{
  return _LHS.eval(v.cast<SMatNumber<scalarD>>()).getSMat();
}
bool PMIConstraint::isDummy(scalarD eps) const
{
  for(sizeType i=0; i<(sizeType)_LHS._terms.size(); i++)
    if(!_LHS._terms[i]._id.empty()) {
      const SMat& m=_LHS._terms[i]._coef.getSMat();
      for(sizeType j=0; j<m.nonZeros(); j++)
        if(std::abs(m.valuePtr()[j])>eps)
          return false;
    }
  return true;
}
sizeType PMIConstraint::nCons() const
{
  return n()*(n()+1)/2;
}
sizeType PMIConstraint::n() const
{
  ASSERT(!_LHS._terms.empty())
  return _LHS._terms[0]._coef.getSMat().rows();
}
PMIConstraint::PolyX& PMIConstraint::LHS()
{
  return _LHS;
}
//SOSProblem
SOSProblem::SOSProblem() {}
SOSProblem::SOSProblem(const SOSProblem& other)
{
  operator=(other);
}
SOSProblem& SOSProblem::operator=(const SOSProblem& other)
{
  copyDoc(other._pt,_pt);
  _solution=other._solution;
  _vars=other._vars;
  _poly=other._poly;
  _pmi=other._pmi;
  return *this;
}
SOSProblem::PolyXA SOSProblem::fullPoly(sizeType nrVar,const std::set<sizeType>* clique,sizeType d) const
{
  PolyXA ret;
  std::vector<TermXA> terms=buildMonomial(nrVar,clique,d*2);
  for(sizeType i=0; i<(sizeType)terms.size(); i++)
    terms[i]._coef=PolyX(TermX(1,i,1));
  std::sort(terms.begin(),terms.end());
  ret._terms=terms;
  return ret;
}
void SOSProblem::makeBSOS(const std::string& nameCons,const std::string& namePoly,const PolyXA& poly,const SemiAlgebraicSet& K,sizeType d,sizeType k)
{
  if(useGraphOrder()==SOSGraph::NONE)
    makeBSOSOnK(nameCons,namePoly,poly,K,NULL,d,k);
  else {
    SOSGraph g(poly,&K,useGraphOrder());
    if(!visualizeCSP().empty()) {
      recreate(visualizeCSP());
      g.writeVTK(visualizeCSP()+"/csp.vtk",visualizeCSP()+"/cspE.vtk");
    }
    makeBSOSOnK(nameCons,namePoly,poly,K,&g,d,k);
  }
}
void SOSProblem::makePutSOS(const std::string& nameCons,const std::string& namePoly,const PolyXA& poly,const SemiAlgebraicSet* K,sizeType d)
{
  if(K) {
    if(useGraphOrder()==SOSGraph::NONE)
      makePutSOSOnK(nameCons,namePoly,poly,*K,NULL,d);
    else {
      SOSGraph g(poly,K,useGraphOrder());
      if(!visualizeCSP().empty()) {
        recreate(visualizeCSP());
        g.writeVTK(visualizeCSP()+"/csp.vtk",visualizeCSP()+"/cspE.vtk");
      }
      makePutSOSOnK(nameCons,namePoly,poly,*K,&g,d);
    }
  } else {
    if(useGraphOrder()==SOSGraph::NONE)
      makeDensePutSOS(nameCons,namePoly,poly,NULL,d);
    else {
      SOSGraph g(poly,K,useGraphOrder());
      if(!visualizeCSP().empty()) {
        recreate(visualizeCSP());
        g.writeVTK(visualizeCSP()+"/csp.vtk",visualizeCSP()+"/cspE.vtk");
      }
      makeSparsePutSOS(nameCons,namePoly,poly,g,d);
    }
  }
}
void SOSProblem::makePutSOSConvex(const std::string& nameCons,const std::string& namePoly,const PolyXA& poly,sizeType d)
{
  sizeType nrVarA=poly.nrVar<'a'>();
  PolyXA::MATP hA=poly.hessianM();
  PolyXA::VECP vA=PolyXA::VECP::Constant(hA.rows(),PolyXA());
  for(sizeType i=0; i<hA.rows(); i++)
    vA[i]=PolyXA(TermXA(ScalarOfT<PolyX>::convert(1),nrVarA+i,1));
  PolyXA polyYTHY=vA.dot(hA*vA);
  makePutSOS(nameCons,namePoly,polyYTHY,NULL,d);
}
void SOSProblem::makePointInside(const std::string& nameCons,const std::string& namePoly,const PolyXA& poly,const Vec& point,scalarD level)
{
  PolyX polyX=ScalarOfT<PolyX>::convert(level)-poly.eval<'a'>(point);
  if(polyX.order<'x'>()==0)
    return;
  else addPolynomialConstraint(nameCons,std::shared_ptr<PolynomialConstraint>(new PolynomialConstraint(*this,namePoly,polyX,false)));
}
void SOSProblem::makeRelaxedVolume(const std::string& nameCons,const std::string& nameConsSOS,const std::string& namePoly)
{
  STrips tripsb;
  PMIS::const_iterator it=_pmi.find(nameConsSOS);
  ASSERT_MSGV(it!=_pmi.end(),"Cannot find constraint: %s!",nameConsSOS.c_str())
  std::shared_ptr<PMIConstraint> pmi(new PMIConstraint);
  sizeType n=it->second->n();
  for(sizeType r=0; r<n; r++) {
    tripsb.push_back(STrip(r,r+n,1));
    tripsb.push_back(STrip(r+n,r,1));
    for(sizeType c=0; c<=r; c++) {
      SOSVariable& rVol=relaxedVolumeVar(namePoly,r,c,MUST_NEW);
      if(r==c)
        rVol._objCoef+=1;  //add objective as trace(V)
      //relaxedVolumeVar
      pmi->LHS()+=PMIConstraint::TermX(EIJSym(n*2,r,c),rVol,1);
      //SOSVar
      pmi->LHS()+=PMIConstraint::TermX(EIJSym(n*2,r+n,c+n),SOSVar(nameConsSOS,r,c,MUST_EXIST),1);
    }
  }
  SMat b;
  b.resize(n*2,n*2);
  b.setFromTriplets(tripsb.begin(),tripsb.end());
  pmi->LHS()+=PMIConstraint::TermX(b);
  addPMIConstraint(nameCons,pmi);
}
void SOSProblem::makeConstraint(const std::string& nameCons,const std::string& namePoly,const PolyX& poly,bool eq)
{
  std::shared_ptr<PolynomialConstraint> c(new PolynomialConstraint(*this,namePoly,poly,eq));
  addPolynomialConstraint(nameCons,c);
}
void SOSProblem::makeObjective(const std::string& namePoly,const PolyX& poly)
{
  ASSERT_MSG(poly.order()<=1,"Objective of SOSProblem must be linear!")
  for(VARMAP::value_type &v:_vars) {
    v.second._objCoef=0;
  }
  for(sizeType i=0; i<(sizeType)poly._terms.size(); i++)
    if(poly._terms[i].order()==1)
      polyVar(namePoly,poly._terms[i]._id[0],MUST_EXIST)._objCoef=poly._terms[i]._coef;
}
void SOSProblem::removeDummyConstraints(scalarD eps)
{
  //poly
  POLYS poly;
  sizeType nrDummy=0;
  for(const POLYS::value_type& v:_poly) {
    if(!v.second->isDummy(eps))
      poly.insert(v);
    else nrDummy++;
  }
  if(outputWhenSolve()) {
    INFOV("Removed %ld/%ld dummy poly constraints!",nrDummy,_poly.size())
  }
  _poly=poly;

  //pmi
  PMIS pmi;
  nrDummy=0;
  for(const PMIS::value_type& v:_pmi) {
    if(!v.second->isDummy(eps))
      pmi.insert(v);
    else nrDummy++;
  }
  if(outputWhenSolve()) {
    INFOV("Removed %ld/%ld dummy pmi constraints!",nrDummy,_pmi.size())
  }
  _pmi=pmi;
}
//property
void SOSProblem::maxIterations(sizeType maxIter)
{
  put<sizeType>(_pt,"maxIterations",maxIter);
}
sizeType SOSProblem::maxIterations() const
{
  return get<sizeType>(_pt,"maxIterations",10000);
}
void SOSProblem::usePolyMonomial(bool use)
{
  put<char>(_pt,"usePolyMonomial",use?1:0);
}
bool SOSProblem::usePolyMonomial() const
{
  return get<char>(_pt,"usePolyMonomial",1);
}
void SOSProblem::useGraphOrder(SOSGraph::ORDER type)
{
  put<char>(_pt,"graphOrder",type);
}
SOSGraph::ORDER SOSProblem::useGraphOrder() const
{
  return (SOSGraph::ORDER)get<char>(_pt,"graphOrder",SOSGraph::NONE);
}
void SOSProblem::checkConsistency(bool check)
{
  put<char>(_pt,"checkConsistency",check?1:0);
}
bool SOSProblem::checkConsistency() const
{
  return get<char>(_pt,"checkConsistency",1);
}
void SOSProblem::visualizeCSP(const std::string& path)
{
  put<std::string>(_pt,"visualizeCSP",path);
}
std::string SOSProblem::visualizeCSP() const
{
  return get<std::string>(_pt,"visualizeCSP","");
}
void SOSProblem::writeFilePath(const std::string& path)
{
  put<std::string>(_pt,"writeFilePath",path);
}
std::string SOSProblem::writeFilePath() const
{
  return get<std::string>(_pt,"writeFilePath","");
}
void SOSProblem::outputWhenSolve(bool output)
{
  put<char>(_pt,"outputWhenSolve",output?1:0);
}
bool SOSProblem::outputWhenSolve() const
{
  return get<char>(_pt,"outputWhenSolve",(char)0);
}
void SOSProblem::solverType(SOLVER_TYPE type)
{
  put<char>(_pt,"solverType",type);
}
SOSProblem::SOLVER_TYPE SOSProblem::solverType() const
{
  return (SOLVER_TYPE)get<char>(_pt,"solverType",SOLVER_MOSEK);
}
void SOSProblem::solverStatus(STATUS_TYPE type)
{
  put<sizeType>(_pt,"solverStatus",type);
}
SOSProblem::STATUS_TYPE SOSProblem::solverStatus() const
{
  return (STATUS_TYPE)get<sizeType>(_pt,"solverStatus",STATUS_ERROR);
}
void SOSProblem::useMultiThreads(bool use)
{
  put<char>(_pt,"useMultiThreads",use?1:0);
}
bool SOSProblem::useMultiThreads() const
{
  return get<char>(_pt,"useMultiThreads",(char)0);
}
//print problem
SOSProblem::operator std::string() const
{
  std::ostringstream oss;
  //objective
  PolyX obj;
  VARMAP_INV invVars;
  oss << "//#variables=" << _vars.size() << std::endl;
  for(const VARMAP::value_type &v:_vars) {
    invVars[v.second._id]=v.first;
    if(v.second._objCoef!=0)
      obj+=TermX(v.second._objCoef,v.second._id,1);
  }
  oss << "//objective[minimize]: " << obj.toString(&invVars) << std::endl;
  //PolynomialConstraint
  sizeType maxLen=0;
  for(const POLYS::value_type &poly:_poly) {
    maxLen=std::max<sizeType>(maxLen,poly.first.length());
  }
  for(const POLYS::value_type &poly:_poly) {
    std::string id=poly.first;
    id.resize(maxLen,' ');
    oss << "//PolynomialConstraint[" << id << "]: ";
    oss << poly.second->toString(invVars);
  }
  //PMIConstraint
  maxLen=0;
  for(const PMIS::value_type &pmi:_pmi) {
    maxLen=std::max<sizeType>(maxLen,pmi.first.length());
  }
  for(const PMIS::value_type &pmi:_pmi) {
    std::string id=pmi.first;
    id.resize(maxLen,' ');
    oss << "//PMIConstraint[" << id << "]: " << std::endl;
    oss << pmi.second->toString(invVars);
  }
  return oss.str();
}
void SOSProblem::writeProb(const std::string& path) const
{
  std::ofstream os(path);
  os << operator std::string();
}
//fix a set of variables and get a new problem
SOSProblem SOSProblem::fix(const std::string& namePoly,const VARMAP_VALUE& vals) const
{
  SOSProblem ret;
  copyDoc(_pt,ret._pt);
  ret._vars=_vars;
  VARMAP_VALUE vMap;
  for(const VARMAP_VALUE::value_type &v:vals) {
    std::string name=polyVarName(namePoly,v.first);
    vMap[_vars.find(name)->second._id]=v.second;
    ret._vars.erase(name);
  }
  sizeType off=0;
  std::vector<sizeType> vars(_vars.size(),-1);
  for(VARMAP::value_type &v:ret._vars) {
    vars[v.second._id]=off;
    v.second._id=off;
    off++;
  }
  for(const POLYS::value_type &v:_poly) {
    std::shared_ptr<PolynomialConstraint> poly(new PolynomialConstraint(v.second->eliminate(vMap)));
    poly->remap(vars);
    ret._poly[v.first]=poly;
  }
  for(const PMIS::value_type &v:_pmi) {
    std::shared_ptr<PMIConstraint> pmi(new PMIConstraint(v.second->eliminate(vMap)));
    pmi->remap(vars);
    ret._pmi[v.first]=pmi;
  }
  return ret;
}
//solve
sizeType SOSProblem::nrPoly() const
{
  return (sizeType)_poly.size();
}
sizeType SOSProblem::nrPMI() const
{
  return (sizeType)_pmi.size();
}
const PolynomialConstraint& SOSProblem::poly(sizeType i) const
{
  sizeType id=0;
  for(const POLYS::value_type &poly:_poly) {
    if(id==i)
      return *(poly.second);
    id++;
  }
  return *(_poly.begin()->second);
}
const PMIConstraint& SOSProblem::PMI(sizeType i) const
{
  sizeType id=0;
  for(const PMIS::value_type &pmi:_pmi) {
    if(id==i)
      return *(pmi.second);
    id++;
  }
  return *(_pmi.begin()->second);
}
SOSProblem::PolyA SOSProblem::evalX(const std::string& name,const PolyXA& poly) const
{
  sizeType nrVarX=poly.nrVar<'x'>();
  Vec x=Vec::Zero(nrVarX);
  for(sizeType i=0; i<nrVarX; i++)
    x[i]=getSolutionOfPolyVar(name,i);
  return poly.eval<'x'>(x);
}
const SOSProblem::Vec& SOSProblem::solution() const
{
  return *_solution;
}
bool SOSProblem::solve()
{
  if(solverType()==SOLVER_MOSEK) {
#ifdef USE_MOSEK_9
    return solveMosek();
#else
    ASSERT_MSG(false,"Mosek9 not supported!")
    return false;
#endif
  } else if(solverType()==SOLVER_SCS) {
#ifdef SCS_SUPPORT
    return solveScs();
#else
    ASSERT_MSG(false,"SCS not supported!")
    return false;
#endif
  } else if(solverType()==SOLVER_SDPA) {
#ifdef SDPA_SUPPORT
    return solveSdpa();
#else
    ASSERT_MSG(false,"SDPA not supported!")
    return false;
#endif
  } else if(solverType()==SOLVER_SDPA_GMP) {
#if defined(SDPA_GMP_SUPPORT)||defined(SDPA_DD_SUPPORT)||defined(SDPA_QD_SUPPORT)
    return solveSdpaGmp();
#else
    ASSERT_MSG(false,"SDPA-GMP not supported!")
    return false;
#endif
  } else {
    ASSERT_MSG(false,"Unknown solver type!")
    return false;
  }
}
//debug
void SOSProblem::debugMonomials(sizeType nrVar,sizeType nrDeg)
{
  INFO("[DebugMonomials] #Var-#Deg:")
  std::vector<std::vector<std::string>> table;
  std::vector<std::string> rowIndices,colIndices;
  for(sizeType v=1; v<=nrVar; v++) {
    rowIndices.push_back(std::to_string(v));
    table.push_back(std::vector<std::string>());
    for(sizeType d=1; d<=nrDeg; d++) {
      std::vector<TermXA> t=SOSProblem().buildMonomial(v,NULL,d);
      table[v-1].push_back(std::to_string(t.size()));
      if(v==1)
        colIndices.push_back(std::to_string(d));
    }
  }
  std::cout << printTable(table,&rowIndices,"#Var",&colIndices,"#Deg") << std::endl;
}
void SOSProblem::debugTerms(const PolyXA& poly)
{
  std::vector<TermXA> terms=SOSProblem().buildMonomial(poly,NULL,(poly.order<'a'>()+1)/2);
  INFOV("[DebugTerms] Half terms in: %s:",poly.toString().c_str())
  for(sizeType i=0; i<(sizeType)terms.size(); i++)
    std::cout << terms[i].toString() << " ";
  std::cout << std::endl;
}
//get variable
scalarD SOSProblem::getSolutionOfPolyVar(const std::string& name,sizeType id) const
{
  return getSolutionOfVar(name+"[X]["+std::to_string(id)+"]");
}
scalarD SOSProblem::getSolutionOfVar(const std::string& nameVar) const
{
  VARMAP::const_iterator it=_vars.find(nameVar);
  ASSERT(_solution && it!=_vars.end())
  return _solution->coeff(it->second);
}
//helper
void SOSProblem::makeUnique(std::vector<TermXA>& t)
{
  sizeType j=0;
  std::sort(t.begin(),t.end());
  for(sizeType i=0; i<(sizeType)t.size(); i++)
    if(i==0 || t[i]!=t[i-1])
      t[j++]=t[i];
  t.resize(j);
}
std::vector<SOSProblem::TermXA> SOSProblem::buildMonomial(const PolyXA& poly,const std::set<sizeType>* clique,sizeType d) const
{
  std::vector<TermXA> ret;
  for(sizeType i=0; i<(sizeType)poly._terms.size(); i++) {
    if(clique) {
      bool validTerm=true;
      const std::vector<sizeType>& ids=poly._terms[i]._id;
      for(sizeType j=0; j<(sizeType)ids.size(); j++)
        if(clique->find(ids[j])==clique->end()) {
          validTerm=false;
          break;
        }
      if(!validTerm)
        continue;
    }
    buildMonomial(ret,poly._terms[i],ScalarOfT<TermXA>::convert(1),0,d);
  }
  makeUnique(ret);
  return ret;
}
void SOSProblem::buildMonomial(std::vector<TermXA>& ret,const TermXA& tPoly,const TermXA& t,sizeType currVar,sizeType d) const
{
  if(currVar>(sizeType)tPoly._id.size()) {
    if(d==0)
      ret.push_back(t);
  } else {
    sizeType dMax=d;
    if(currVar>0)
      dMax=std::min(d,tPoly._order[currVar-1]);
    for(sizeType dVar=0; dVar<=dMax; dVar++) {
      TermXA t2=t;
      if(currVar>0 && dVar>0) {
        t2._id.push_back(tPoly._id[currVar-1]);
        t2._order.push_back(dVar);
      }
      buildMonomial(ret,tPoly,t2,currVar+1,d-dVar);
    }
  }
}
std::vector<SOSProblem::TermXA> SOSProblem::buildMonomial(sizeType nrVarY,const std::set<sizeType>* clique,sizeType d) const
{
  std::vector<TermXA> ret;
  buildMonomial(ret,ScalarOfT<TermXA>::convert(1),0,nrVarY,d);
  if(clique) {
    ASSERT(nrVarY==(sizeType)clique->size())
    std::vector<sizeType> cliqueVec(clique->begin(),clique->end());
    for(sizeType i=0; i<(sizeType)ret.size(); i++)
      for(sizeType j=0; j<(sizeType)ret[i]._id.size(); j++)
        ret[i]._id[j]=cliqueVec[ret[i]._id[j]];
  }
  makeUnique(ret);
  return ret;
}
void SOSProblem::buildMonomial(std::vector<TermXA>& ret,const TermXA& t,sizeType currVar,sizeType nrVar,sizeType d) const
{
  if(currVar>nrVar) {
    if(d==0)
      ret.push_back(t);
  } else {
    for(sizeType dVar=0; dVar<=d; dVar++) {
      TermXA t2=t;
      if(currVar>0 && dVar>0) {
        t2._id.push_back(currVar-1);
        t2._order.push_back(dVar);
      }
      buildMonomial(ret,t2,currVar+1,nrVar,d-dVar);
    }
  }
}
//Problem based on Krivine-Stengle-Putinar's Positivstellensatz
void SOSProblem::makeBSOSOnK(const std::string& nameCons,const std::string& namePoly,const PolyXA& poly,const SemiAlgebraicSet& K,const SOSGraph* g,sizeType d,sizeType k)
{
  if(g)
    makeSparseBSOS(nameCons,namePoly,poly,K,*g,d,k);
  else makeDenseBSOS(nameCons,namePoly,poly,K,d,k);
}
void SOSProblem::makeDenseBSOS(const std::string& nameCons,const std::string& namePoly,PolyXA poly,const SemiAlgebraicSet& K,sizeType d,sizeType k)
{
  //Lagrangian
  buildLagrangianBSOS(nameCons,namePoly,poly,K,NULL,d);
  //create equality constraints
  std::map<TermXA,std::shared_ptr<PolynomialConstraint>> mCMap;
  std::vector<TermXA> rowIndices;
  if(usePolyMonomial())
    rowIndices=buildMonomial(poly,NULL,k);
  else rowIndices=buildMonomial(poly.nrVar<'a'>(),NULL,k);
  std::shared_ptr<PMIConstraint> pmi(new PMIConstraint);
  sizeType n=(sizeType)rowIndices.size(),rRemaining=n;
  if(n>0) {
    for(sizeType r=0; r<n; r++)
      for(sizeType c=0; c<=r; c++) {
        pmi->LHS()+=PMIConstraint::TermX(EIJSym(n,r,c),SOSVar(nameCons,r,c,MUST_NEW),1);
        //find term
        TermXA t=rowIndices[r]*rowIndices[c];
        if(mCMap.find(t)==mCMap.end()) {
          std::vector<TermXA>::const_iterator it=std::lower_bound(poly._terms.begin(),poly._terms.end(),t);
          if(it==poly._terms.end() || *it!=t) {
            std::shared_ptr<PolynomialConstraint> cons(new PolynomialConstraint(0,true));
            addPolynomialConstraint(SOSVarName(nameCons,r,c),cons);
            mCMap[t]=cons;
          } else {
            std::shared_ptr<PolynomialConstraint> cons(new PolynomialConstraint(*this,namePoly,it->_coef,true));
            addPolynomialConstraint(SOSVarName(nameCons,r,c),cons);
            mCMap[t]=cons;
          }
        }
        mCMap.find(t)->second->LHS()-=TermX(r==c?1:2,SOSVar(nameCons,r,c,MUST_EXIST),1);
      }
    addPMIConstraint(nameCons,pmi);
  }
  //check consistency (in BSOS, some terms in poly might not be covered by SOSVars. We set these terms to zero.)
  for(sizeType i=0; i<(sizeType)poly._terms.size(); i++)
    if(mCMap.find(poly._terms[i])==mCMap.end()) {
      std::shared_ptr<PolynomialConstraint> cons(new PolynomialConstraint(*this,namePoly,poly._terms[i]._coef,true));
      addPolynomialConstraint(SOSVarName(nameCons,rRemaining++),cons);
    }
}
void SOSProblem::makeSparseBSOS(const std::string& nameCons,const std::string& namePoly,PolyXA poly,const SemiAlgebraicSet& K,const SOSGraph& g,sizeType d,sizeType k)
{
  //Lagrangian
  buildLagrangianBSOS(nameCons,namePoly,poly,K,&g,d);
  //create equality constraints
  sizeType rRemaining=0;
  std::map<TermXA,std::shared_ptr<PolynomialConstraint>> mCMap;
  const std::vector<std::set<sizeType>>& cliques=g.getMaximalCliques();
  for(sizeType i=0; i<(sizeType)cliques.size(); i++) {
    const std::set<sizeType>& clique=cliques[i];
    const std::string nameConsCi=nameCons+"[c"+std::to_string(i)+"]";
    //create equality constraints
    std::vector<TermXA> rowIndices;
    if(usePolyMonomial())
      rowIndices=buildMonomial(poly,&clique,k);
    else rowIndices=buildMonomial((sizeType)clique.size(),&clique,k);
    std::shared_ptr<PMIConstraint> pmi(new PMIConstraint);
    sizeType n=(sizeType)rowIndices.size();
    rRemaining+=n;
    if(n>0) {
      for(sizeType r=0; r<n; r++)
        for(sizeType c=0; c<=r; c++) {
          pmi->LHS()+=PMIConstraint::TermX(EIJSym(n,r,c),SOSVar(nameConsCi,r,c,MUST_NEW),1);
          //find term
          TermXA t=rowIndices[r]*rowIndices[c];
          if(mCMap.find(t)==mCMap.end()) {
            std::vector<TermXA>::const_iterator it=std::lower_bound(poly._terms.begin(),poly._terms.end(),t);
            if(it==poly._terms.end() || *it!=t) {
              std::shared_ptr<PolynomialConstraint> cons(new PolynomialConstraint(0,true));
              addPolynomialConstraint(SOSVarName(nameConsCi,r,c),cons);
              mCMap[t]=cons;
            } else {
              std::shared_ptr<PolynomialConstraint> cons(new PolynomialConstraint(*this,namePoly,it->_coef,true));
              addPolynomialConstraint(SOSVarName(nameConsCi,r,c),cons);
              mCMap[t]=cons;
            }
          }
          mCMap.find(t)->second->LHS()-=TermX(r==c?1:2,SOSVar(nameConsCi,r,c,MUST_EXIST),1);
        }
      addPMIConstraint(nameConsCi,pmi);
    }
  }
  //check consistency (in BSOS, some terms in poly might not be covered by SOSVars. We set these terms to zero.)
  for(sizeType i=0; i<(sizeType)poly._terms.size(); i++)
    if(mCMap.find(poly._terms[i])==mCMap.end()) {
      std::shared_ptr<PolynomialConstraint> cons(new PolynomialConstraint(*this,namePoly,poly._terms[i]._coef,true));
      addPolynomialConstraint(SOSVarName(nameCons,rRemaining++),cons);
    }
}
void SOSProblem::buildLagrangianBSOS(const std::string& nameCons,const std::string& namePoly,PolyXA& poly,const SemiAlgebraicSet& K,const SOSGraph* g,sizeType d)
{
//#define USE_KPRIME_IN_BSOS
#ifdef USE_KPRIME_IN_BSOS
  //KPrime
  SemiAlgebraicSet KPrime;
  for(sizeType i=0; i<K._nrEq; i++) {
    KPrime._f.push_back(K._f[i]);
    KPrime._f.push_back(K._f[i]*ScalarOfT<PolyXA>::convert(-1));
  }
  for(sizeType i=K._nrEq; i<(sizeType)K._f.size(); i++)
    KPrime._f.push_back(K._f[i]);
#else
  const SemiAlgebraicSet& KPrime=K;
#endif
  //count var
  sizeType nrVarX=nrVarAll<'x'>(poly,KPrime);
  std::vector<PolyXA> Lags;
  if(g) {
    //cluster f into cliques
    const std::vector<std::set<sizeType>>& cliques=g->getMaximalCliques();
    std::vector<SemiAlgebraicSet> KCliques(cliques.size());
    for(sizeType i=0; i<(sizeType)KPrime._nrEq; i++) {
      std::vector<sizeType> ids=g->getCliqueIdAll(KPrime._f[i]);
      for(sizeType j=0; j<(sizeType)ids.size(); j++)
        KCliques[ids[j]]._f.push_back(KPrime._f[i]);
    }
    for(sizeType i=0; i<(sizeType)KCliques.size(); i++)
      KCliques[i]._nrEq=(sizeType)KCliques[i]._f.size();
    for(sizeType i=KPrime._nrEq; i<(sizeType)KPrime._f.size(); i++) {
      std::vector<sizeType> ids=g->getCliqueIdAll(KPrime._f[i]);
      for(sizeType j=0; j<(sizeType)ids.size(); j++)
        KCliques[ids[j]]._f.push_back(KPrime._f[i]);
    }
    //Lagrangian
    for(sizeType i=0; i<(sizeType)cliques.size(); i++)
      if(!KCliques[i]._f.empty())
        buildLagrangianBSOS(nameCons,namePoly,Lags,ScalarOfT<PolyXA>::convert(-1),KCliques[i],nrVarX,0,d,true);
  } else {
    //Lagrangian
    buildLagrangianBSOS(nameCons,namePoly,Lags,ScalarOfT<PolyXA>::convert(-1),KPrime,nrVarX,0,d,true);
  }
  //sum up
  Lags.push_back(poly);
  poly.sum(Lags);
}
void SOSProblem::buildLagrangianBSOS(const std::string& nameCons,const std::string& namePoly,std::vector<PolyXA>& Lags,PolyXA p,const SemiAlgebraicSet& K,sizeType& nrVarX,sizeType currF,sizeType d,bool mustPositive)
{
  if(currF>(sizeType)K._f.size()) {
    if(d==0) {
      PolyXA L=ScalarOfT<PolyXA>::convertVar(nrVarX,'x');
      if(mustPositive) {
        std::shared_ptr<PolynomialConstraint> cons(new PolynomialConstraint(*this,namePoly,L,false));
        addPolynomialConstraint(nameCons+"[L"+std::to_string(nrVarX)+"]",cons);
      }
      Lags.push_back(L*p);
      nrVarX++;
    }
  } else {
    for(sizeType dVar=0; dVar<=d; dVar++) {
      if(currF>0 && dVar>0) {
        p*=K._f[currF-1];
#ifndef USE_KPRIME_IN_BSOS
        if(currF-1<K._nrEq)
          mustPositive=false;
#endif
      }
      buildLagrangianBSOS(nameCons,namePoly,Lags,p,K,nrVarX,currF+1,d-dVar,mustPositive);
    }
  }
}
//Problem based on Putinar's Positivstellensatz
void SOSProblem::makePutSOSOnK(const std::string& nameCons,const std::string& namePoly,const PolyXA& poly,const SemiAlgebraicSet& K,const SOSGraph* g,sizeType d)
{
  //count degree
  d=std::max(d,(poly.order<'a'>()+1)/2);
  for(sizeType i=0; i<(sizeType)K._f.size(); i++)
    d=std::max(d,(K._f[i].order<'a'>()+1)/2);
  //build PutSOS
  PolyXA polyOnK=poly;
  sizeType nrX=poly.nrVar<'x'>(),nrXNew;
  for(sizeType i=0; i<(sizeType)K._f.size(); i++) {
    std::set<sizeType> clique;
    PolyXA L;
    if(g) {
      clique=g->getClique(K._f[i]);
      L=fullPoly((sizeType)clique.size(),&clique,d-(K._f[i].order<'a'>()+1)/2);
    } else {
      L=fullPoly(poly.nrVar<'a'>(),NULL,d-(K._f[i].order<'a'>()+1)/2);
    }
    nrXNew=L.nrVar<'x'>();
    PolyXA LT=L.affineTransXId<'x'>(1,nrX);
    polyOnK-=LT*K._f[i];
    nrX+=nrXNew;
    //Lagrangian cone
    if(i>=K._nrEq) {
      if(g)
        makeDensePutSOS(nameCons+"[L"+std::to_string(i)+"]",namePoly,LT,&clique,0);
      else makeDensePutSOS(nameCons+"[L"+std::to_string(i)+"]",namePoly,LT,NULL,0);
    }
  }
  if(g)
    makeSparsePutSOS(nameCons,namePoly,polyOnK,*g,d);
  else makeDensePutSOS(nameCons,namePoly,polyOnK,NULL,d);
}
void SOSProblem::makeDensePutSOS(const std::string& nameCons,const std::string& namePoly,const PolyXA& poly,const std::set<sizeType>* clique,sizeType d)
{
  d=std::max(d,(poly.order<'a'>()+1)/2);
  std::map<TermXA,std::shared_ptr<PolynomialConstraint>> mCMap;
  //create equality constraints
  std::vector<TermXA> rowIndices;
  if(usePolyMonomial())
    rowIndices=buildMonomial(poly,clique,d);
  else rowIndices=buildMonomial(poly.nrVar<'a'>(),clique,d);
  std::shared_ptr<PMIConstraint> pmi(new PMIConstraint);
  sizeType n=(sizeType)rowIndices.size();
  if(n==0)
    return;
  for(sizeType r=0; r<n; r++)
    for(sizeType c=0; c<=r; c++) {
      pmi->LHS()+=PMIConstraint::TermX(EIJSym(n,r,c),SOSVar(nameCons,r,c,MUST_NEW),1);
      //find term
      TermXA t=rowIndices[r]*rowIndices[c];
      if(mCMap.find(t)==mCMap.end()) {
        std::vector<TermXA>::const_iterator it=std::lower_bound(poly._terms.begin(),poly._terms.end(),t);
        if(it==poly._terms.end() || *it!=t) {
          std::shared_ptr<PolynomialConstraint> cons(new PolynomialConstraint(0,true));
          addPolynomialConstraint(SOSVarName(nameCons,r,c),cons);
          mCMap[t]=cons;
        } else {
          std::shared_ptr<PolynomialConstraint> cons(new PolynomialConstraint(*this,namePoly,it->_coef,true));
          addPolynomialConstraint(SOSVarName(nameCons,r,c),cons);
          mCMap[t]=cons;
        }
      }
      mCMap.find(t)->second->LHS()-=TermX(r==c?1:2,SOSVar(nameCons,r,c,MUST_EXIST),1);
    }
  addPMIConstraint(nameCons,pmi);
  //check consistency
  if(checkConsistency())
    for(sizeType i=0; i<(sizeType)poly._terms.size(); i++) {
      ASSERT_MSGV(mCMap.find(poly._terms[i])!=mCMap.end(),"DenseSOS[%s] does not cover poly[%s]'s term: %s!",nameCons.c_str(),namePoly.c_str(),poly._terms[i].toString().c_str())
    }
}
void SOSProblem::makeSparsePutSOS(const std::string& nameCons,const std::string& namePoly,const PolyXA& poly,const SOSGraph& g,sizeType d)
{
  d=std::max(d,(poly.order<'a'>()+1)/2);
  std::map<TermXA,std::shared_ptr<PolynomialConstraint>> mCMap;
  const std::vector<std::set<sizeType>>& cliques=g.getMaximalCliques();
  for(sizeType i=0; i<(sizeType)cliques.size(); i++) {
    const std::set<sizeType>& clique=cliques[i];
    const std::string nameConsCi=nameCons+"[c"+std::to_string(i)+"]";
    //create equality constraints
    std::vector<TermXA> rowIndices;
    if(usePolyMonomial())
      rowIndices=buildMonomial(poly,&clique,d);
    else rowIndices=buildMonomial((sizeType)clique.size(),&clique,d);
    std::shared_ptr<PMIConstraint> pmi(new PMIConstraint);
    sizeType n=(sizeType)rowIndices.size();
    if(n==0)
      continue;
    for(sizeType r=0; r<n; r++)
      for(sizeType c=0; c<=r; c++) {
        pmi->LHS()+=PMIConstraint::TermX(EIJSym(n,r,c),SOSVar(nameConsCi,r,c,MUST_NEW),1);
        //find term
        TermXA t=rowIndices[r]*rowIndices[c];
        if(mCMap.find(t)==mCMap.end()) {
          std::vector<TermXA>::const_iterator it=std::lower_bound(poly._terms.begin(),poly._terms.end(),t);
          if(it==poly._terms.end() || *it!=t) {
            std::shared_ptr<PolynomialConstraint> cons(new PolynomialConstraint(0,true));
            addPolynomialConstraint(SOSVarName(nameConsCi,r,c),cons);
            mCMap[t]=cons;
          } else {
            std::shared_ptr<PolynomialConstraint> cons(new PolynomialConstraint(*this,namePoly,it->_coef,true));
            addPolynomialConstraint(SOSVarName(nameConsCi,r,c),cons);
            mCMap[t]=cons;
          }
        }
        mCMap.find(t)->second->LHS()-=TermX(r==c?1:2,SOSVar(nameConsCi,r,c,MUST_EXIST),1);
      }
    addPMIConstraint(nameConsCi,pmi);
  }
  //check consistency
  if(checkConsistency())
    for(sizeType i=0; i<(sizeType)poly._terms.size(); i++) {
      ASSERT_MSGV(mCMap.find(poly._terms[i])!=mCMap.end(),"SparseSOS[%s] does not cover poly[%s]'s term: %s!",nameCons.c_str(),namePoly.c_str(),poly._terms[i].toString().c_str())
    }
}
//decl. variable
std::string SOSProblem::SOSVarName(const std::string& name,sizeType r) const
{
  return name+"[SOS]["+std::to_string(r)+"]";
}
std::string SOSProblem::SOSVarName(const std::string& name,sizeType r,sizeType c) const
{
  return SOSVarName(name,r)+"["+std::to_string(c)+"]";
}
std::string SOSProblem::relaxedVolumeVarName(const std::string& name,sizeType r,sizeType c) const
{
  return name+"[RVol]["+std::to_string(r)+"]["+std::to_string(c)+"]";
}
std::string SOSProblem::polyVarName(const std::string& name,sizeType id) const
{
  return name+"[X]["+std::to_string(id)+"]";
}
SOSVariable& SOSProblem::SOSVar(const std::string& name,sizeType r,VARIABLE_OP op)
{
  return var(SOSVarName(name,r),op);
}
SOSVariable& SOSProblem::SOSVar(const std::string& name,sizeType r,sizeType c,VARIABLE_OP op)
{
  return var(SOSVarName(name,r,c),op);
}
SOSVariable& SOSProblem::relaxedVolumeVar(const std::string& name,sizeType r,sizeType c,VARIABLE_OP op)
{
  return var(relaxedVolumeVarName(name,r,c),op);
}
SOSVariable& SOSProblem::polyVar(const std::string& name,sizeType id,VARIABLE_OP op)
{
  return var(polyVarName(name,id),op);
}
SOSVariable& SOSProblem::var(const std::string& nameVar,VARIABLE_OP op)
{
  VARMAP::iterator it=_vars.find(nameVar);
  if(it!=_vars.end()) {
    ASSERT_MSGV(op!=MUST_NEW,"Variable: %s already exists!",nameVar.c_str())
    return it->second;
  } else {
    ASSERT_MSGV(op!=MUST_EXIST,"Variable: %s does not exist!",nameVar.c_str())
    sizeType id=(sizeType)_vars.size();
    return (_vars[nameVar]=SOSVariable(id,0));
  }
}
//constraint
void SOSProblem::addPolynomialConstraint(const std::string& nameCons,std::shared_ptr<PolynomialConstraint> c)
{
  ASSERT_MSGV(_poly.find(nameCons)==_poly.end(),"PolynomialConstraint: %s already exists!",nameCons.c_str())
  _poly[nameCons]=c;
}
void SOSProblem::addPMIConstraint(const std::string& nameCons,std::shared_ptr<PMIConstraint> c)
{
  ASSERT_MSGV(_pmi.find(nameCons)==_pmi.end(),"PMIConstraint: %s already exists!",nameCons.c_str())
  _pmi[nameCons]=c;
}
SOSProblem::SMat SOSProblem::EIJSym(sizeType n,sizeType r,sizeType c) const
{
  SMat ret;
  ret.resize(n,n);
  ret.coeffRef(r,c)=ret.coeffRef(c,r)=1;
  return ret;
}
//solve
#ifdef USE_MOSEK_9
bool SOSProblem::solveMosek()
{
  MSKrescodee r;
  _solution=NULL;
  MSKenv_t env=NULL;
  MSKtask_t task=NULL;
  MSKint32t NUMCON=_poly.size();
  MSKint32t NUMVAR=_vars.size();
  MSKint32t NUMBARVAR=_pmi.size();
  std::vector<MSKint32t> DIMBARVAR;
  for(const PMIS::value_type &v:_pmi) {
    DIMBARVAR.push_back(v.second->n());
    NUMCON+=v.second->nCons();
  }
  SAFE_CALL(MSK_makeenv(&env,NULL))
  SAFE_CALL(MSK_maketask(env,NUMCON,0,&task))
  SAFE_CALL(MSK_putintparam(task,MSK_IPAR_PRESOLVE_USE,MSK_PRESOLVE_MODE_ON))
  SAFE_CALL(MSK_putintparam(task,MSK_IPAR_INTPNT_MAX_ITERATIONS,maxIterations()))
  SAFE_CALL(MSK_putintparam(task,MSK_IPAR_INTPNT_MULTI_THREAD,useMultiThreads()?MSK_ON:MSK_OFF))
  if(outputWhenSolve())
    MSK_linkfunctotaskstream(task,MSK_STREAM_LOG,NULL,[](void *handle,const char str[]) {
    printf("%s",str);
  });
  else MSK_linkfunctotaskstream(task,MSK_STREAM_LOG,NULL,[](void *handle,const char str[]) {});
  SAFE_CALL(MSK_appendcons(task,NUMCON))
  SAFE_CALL(MSK_appendvars(task,NUMVAR))
  SAFE_CALL(MSK_appendbarvars(task,NUMBARVAR,&DIMBARVAR[0]))
  //var ranges
  for(MSKint32t j=0; j<NUMVAR; ++j) {
    SAFE_CALL(MSK_putvarbound(task,j,MSK_BK_FR,-MSK_INFINITY,MSK_INFINITY))
  }
  //objectives
  SAFE_CALL(MSK_putcfix(task,0))
  SAFE_CALL(MSK_putobjsense(task,MSK_OBJECTIVE_SENSE_MINIMIZE))
  for(const VARMAP::value_type &v:_vars) {
    if(v.second._objCoef!=0) {
      SAFE_CALL(MSK_putcj(task,v.second._id,v.second._objCoef))
    }
  }
  MSKint32t cid=0,pmiId=0;
  for(const POLYS::value_type &poly:_poly) {
    if(!poly.second->setToMosek(cid,env,task))
      return false;
  }
  for(const PMIS::value_type &pmi:_pmi) {
    if(!pmi.second->setToMosek(pmiId,cid,env,task))
      return false;
    pmiId++;
  }
  ASSERT(cid==NUMCON && pmiId==NUMBARVAR)
  //solve
  pmiId=0;
  bool ret=false;
  MSKsolstae solsta;
  MSKrealt* xx=NULL;
  //MSKrealt* barxx=NULL;
  MSKrescodee trmcode;
  if(!writeFilePath().empty()) {
    for(const VARMAP::value_type &v:_vars) {
      SAFE_CALL(MSK_putvarname(task,v.second._id,v.first.c_str()))
    }
    SAFE_CALL(MSK_writedata(task,writeFilePath().c_str()))
  }
  SAFE_CALL(MSK_optimizetrm(task,&trmcode))
  MSK_solutionsummary(task,MSK_STREAM_MSG);
  MSK_getsolsta(task,MSK_SOL_ITR,&solsta);
  switch(solsta) {
  case MSK_SOL_STA_OPTIMAL:
    //get xx
    xx=(MSKrealt*)MSK_calloctask(task,NUMVAR,sizeof(MSKrealt));
    SAFE_CALL(MSK_getxx(task,MSK_SOL_ITR,xx))
    _solution.reset(new Vec(Eigen::Map<Eigen::Matrix<MSKrealt,-1,1>>(xx,NUMVAR).cast<scalarD>()));
    MSK_freetask(task,xx);
    //get barx
    /*_pmiMosek.resize(_pmi.size());
    BOOST_FOREACH(const PMIS::value_type &pmi,_pmi) {
      barxx=(MSKrealt*)MSK_calloctask(task,pmi.second->nCons(),sizeof(MSKrealt));
      SAFE_CALL(MSK_getbarxj(task,MSK_SOL_ITR,pmiId,barxx))
      DMat& dMat=_pmiMosek[pmiId];
      dMat.resize(pmi.second->n(),pmi.second->n());
      for(sizeType c=0,off=0; c<pmi.second->n(); c++)
        for(sizeType r=c; r<pmi.second->n(); r++)
          dMat(r,c)=dMat(c,r)=barxx[off++];
      MSK_freetask(task,barxx);
      pmiId++;
    }*/
    solverStatus(STATUS_OPTIMAL);
    ret=true;
    break;
  case MSK_SOL_STA_DUAL_INFEAS_CER:
    solverStatus(STATUS_UNBOUNDED);
    break;
  case MSK_SOL_STA_PRIM_INFEAS_CER:
    solverStatus(STATUS_INFEASIBLE);
    break;
  default:
    solverStatus(STATUS_ERROR);
    break;
  }
  //if(!ret) {
  //  std::filesystem::ofstream os("ErrProb.dat");
  //  os << operator std::string();
  //}
  //finalize
  if(task)
    MSK_deletetask(&task);
  if(env)
    MSK_deleteenv(&env);
  return ret;
}
#endif
#ifdef SCS_SUPPORT
bool SOSProblem::solveScs()
{
  _solution=NULL;
  ScsInterface scs;
  Vec c=Vec::Zero((sizeType)_vars.size());
  Vec x=Vec::Zero((sizeType)_vars.size());
  //objectives
  for(const VARMAP::value_type &v:_vars) {
    if(v.second._objCoef!=0)
      c[v.second._id]=v.second._objCoef;
  }
  for(const POLYS::value_type &poly:_poly) {
    poly.second->setToScs(scs,(sizeType)_vars.size());
  }
  for(const PMIS::value_type &pmi:_pmi) {
    pmi.second->setToScs(scs,(sizeType)_vars.size());
  }
  //solve
  sizeType ret=scs.solve(c,x,outputWhenSolve(),maxIterations());
  _solution.reset(new Vec(x));
  if(ret>0)
    solverStatus(STATUS_OPTIMAL);
  else if(ret==SCS_UNBOUNDED || ret==SCS_UNBOUNDED_INACCURATE)
    solverStatus(STATUS_UNBOUNDED);
  else if(ret==SCS_INFEASIBLE || ret==SCS_INFEASIBLE_INACCURATE)
    solverStatus(STATUS_INFEASIBLE);
  else solverStatus(STATUS_ERROR);
  return ret>0;
}
#endif
#ifdef SDPA_SUPPORT
bool SOSProblem::solveSdpa()
{
  SDPA sdpa;
  _solution=NULL;
  sdpa.setParameterType(SDPA::PARAMETER_DEFAULT);
  sdpa.setParameterMaxIteration(maxIterations());
  sdpa.inputConstraintNumber(_vars.size());
  sdpa.inputBlockNumber(_poly.size()+_pmi.size());
  //prepare
  sizeType cid=1;
  for(const POLYS::value_type &poly:_poly) {
    poly.second->setToSdpa(sdpa,cid,true);
  }
  for(const PMIS::value_type &pmi:_pmi) {
    pmi.second->setToSdpa(sdpa,cid,true);
  }
  sdpa.initializeUpperTriangleSpace();
  //objective
  for(const VARMAP::value_type &v:_vars) {
    if(v.second._objCoef!=0)
      sdpa.inputCVec(v.second._id+1,v.second._objCoef);
  }
  //set data
  cid=1;
  for(const POLYS::value_type &poly:_poly) {
    poly.second->setToSdpa(sdpa,cid,false);
  }
  for(const PMIS::value_type &pmi:_pmi) {
    pmi.second->setToSdpa(sdpa,cid,false);
  }
  sdpa.initializeUpperTriangle();
  sdpa.initializeSolve();
  sdpa.setDisplay(outputWhenSolve()?stdout:NULL);
  sdpa.solve();
  Vec sol=Vec::Zero((sizeType)_vars.size());
  for(sizeType i=0; i<(sizeType)_vars.size(); i++)
    sol[i]=sdpa.getResultXVec()[i];
  _solution.reset(new Vec(sol));
  if(sdpa.getPhaseValue()==SDPA::pdOPT)
    solverStatus(STATUS_OPTIMAL);
  else if(sdpa.getPhaseValue()==SDPA::pUNBD)
    solverStatus(STATUS_UNBOUNDED);
  else if(sdpa.getPhaseValue()==SDPA::pdINF || sdpa.getPhaseValue()==SDPA::pINF_dFEAS)
    solverStatus(STATUS_INFEASIBLE);
  else solverStatus(STATUS_ERROR);
  return sdpa.getPhaseValue()==SDPA::pdOPT;
}
#endif
#if defined(SDPA_GMP_SUPPORT)||defined(SDPA_DD_SUPPORT)||defined(SDPA_QD_SUPPORT)
bool SOSProblem::solveSdpaGmp()
{
  SDPAGMP sdpa;
  _solution=NULL;
  sdpa.setParameterType(SDPAGMP::PARAMETER_DEFAULT);
  sdpa.setParameterMaxIteration(maxIterations());
  sdpa.inputConstraintNumber(_vars.size());
  //count #constraints
  sizeType cid=(sizeType)_pmi.size();
  for(const POLYS::value_type &poly:_poly) {
    cid+=poly.second->eq()?2:1;
  }
  sdpa.inputBlockNumber(cid);
  //prepare
  cid=1;
  for(const POLYS::value_type &poly:_poly) {
    poly.second->setToSdpaGmp(sdpa,cid,true);
  }
  for(const PMIS::value_type &pmi:_pmi) {
    pmi.second->setToSdpaGmp(sdpa,cid,true);
  }
  sdpa.initializeUpperTriangleSpace();
  //objective
  for(const VARMAP::value_type &v:_vars) {
    if(v.second._objCoef!=0)
      sdpa.inputCVec(v.second._id+1,v.second._objCoef);
  }
  //set data
  cid=1;
  for(const POLYS::value_type &poly:_poly) {
    poly.second->setToSdpaGmp(sdpa,cid,false);
  }
  for(const PMIS::value_type &pmi:_pmi) {
    pmi.second->setToSdpaGmp(sdpa,cid,false);
  }
  sdpa.initializeUpperTriangle();
  sdpa.initializeSolve();
  sdpa.setDisplay(outputWhenSolve()?stdout:NULL);
  sdpa.solve();
  Vec sol=Vec::Zero((sizeType)_vars.size());
  for(sizeType i=0; i<(sizeType)_vars.size(); i++)
    sol[i]=-sdpa.get_d(sdpa.getResultXVec()[i]);
  _solution.reset(new Vec(sol));
  if(sdpa.getPhaseValue()==SDPAGMP::pdOPT)
    solverStatus(STATUS_OPTIMAL);
  else if(sdpa.getPhaseValue()==SDPAGMP::pUNBD)
    solverStatus(STATUS_UNBOUNDED);
  else if(sdpa.getPhaseValue()==SDPAGMP::pdINF || sdpa.getPhaseValue()==SDPAGMP::pINF_dFEAS)
    solverStatus(STATUS_INFEASIBLE);
  else solverStatus(STATUS_ERROR);
  return sdpa.getPhaseValue()==SDPAGMP::pdOPT;
}
#endif
