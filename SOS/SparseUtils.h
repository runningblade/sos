#ifndef SPARSE_UTILS_H
#define SPARSE_UTILS_H

#include <Eigen/Eigen>
#include <Eigen/Sparse>
#include <CommonFile/IO.h>
#include "ParallelVector.h"

PRJ_BEGIN

//sparse matrix building
//dense
template <typename MAT,typename Derived>
static void addBlock(MAT& H,sizeType r,sizeType c,const Eigen::MatrixBase<Derived>& coef)
{
  H.block(r,c,coef.rows(),coef.cols())+=coef;
}
template <typename MAT>
static void addBlock(MAT& H,sizeType r,sizeType c,scalarD coef)
{
  H(r,c)+=coef;
}
template <typename MAT,typename T>
static void addBlockId(MAT& H,sizeType r,sizeType c,sizeType nr,T coefId)
{
  H.block(r,c,nr,nr).diagonal().array()+=coefId;
}
//sparse
template <typename Derived>
static void addBlock(ParallelVector<Eigen::Triplet<scalarD,sizeType> >& H,sizeType r,sizeType c,const Eigen::MatrixBase<Derived>& coef)
{
  sizeType nrR=coef.rows();
  sizeType nrC=coef.cols();
  for(sizeType i=0; i<nrR; i++)
    for(sizeType j=0; j<nrC; j++)
      H.push_back(Eigen::Triplet<scalarD,sizeType>(r+i,c+j,coef(i,j)));
}
static void addBlock(ParallelVector<Eigen::Triplet<scalarD,sizeType> >& H,sizeType r,sizeType c,scalarD coef)
{
  H.push_back(Eigen::Triplet<scalarD,sizeType>(r,c,coef));
}
template <typename Derived>
static void addBlockI(ParallelVector<Eigen::Triplet<scalarD,sizeType> >& H,sizeType r,sizeType c,const Eigen::MatrixBase<Derived>& coefI)
{
  sizeType nr=coefI.size();
  for(sizeType i=0; i<nr; i++)
    H.push_back(Eigen::Triplet<scalarD,sizeType>(r+i,c+i,coefI[i]));
}
template <typename T>
static void addBlockId(ParallelVector<Eigen::Triplet<scalarD,sizeType> >& H,sizeType r,sizeType c,sizeType nr,T coefId)
{
  for(sizeType i=0; i<nr; i++)
    H.push_back(Eigen::Triplet<scalarD,sizeType>(r+i,c+i,coefId));
}
template <typename S,int O,typename I>
static void addBlock(ParallelVector<Eigen::Triplet<scalarD,sizeType> >& H,sizeType r,sizeType c,const Eigen::SparseMatrix<S,O,I>& coef)
{
  for(sizeType k=0; k<coef.outerSize(); ++k)
    for(typename Eigen::SparseMatrix<S,O,I>::InnerIterator it(coef,k); it; ++it)
      H.push_back(Eigen::Triplet<scalarD,sizeType>(r+it.row(),c+it.col(),it.value()));
}
//sparseVec
template <typename Derived>
static void addBlock(Eigen::SparseVector<scalarD,0,sizeType>& H,sizeType r,const Eigen::MatrixBase<Derived>& coef)
{
  for(sizeType i=0; i<coef.rows(); i++)
    H.coeffRef(r+i)+=coef[i];
}
//maxAbs
scalarD absMax(const Eigen::SparseMatrix<scalarD,0,sizeType>& h);
scalarD absMaxRel(const Eigen::SparseMatrix<scalarD,0,sizeType>& h,const Eigen::SparseMatrix<scalarD,0,sizeType>& hRef,bool detail=false);
//build KKT matrix
Matd buildKKT(const Matd& h,const Matd& a,scalarD shift);
Eigen::SparseMatrix<scalarD,0,sizeType> buildKKT(const Eigen::SparseMatrix<scalarD,0,sizeType>& h,const Eigen::SparseMatrix<scalarD,0,sizeType>& a,scalarD shift);
//kronecker-product
Eigen::SparseMatrix<scalarD,0,sizeType> kronecker(const Eigen::SparseMatrix<scalarD,0,sizeType>& h,sizeType n);
//concat-diag
Eigen::SparseMatrix<scalarD,0,sizeType> concatDiag(const Eigen::SparseMatrix<scalarD,0,sizeType>& a,const Eigen::SparseMatrix<scalarD,0,sizeType>& b);
//sparseIO
template <typename T,int O,typename I>
bool readBinaryData(Eigen::SparseMatrix<T,O,I>& m,std::istream& is,IOData* dat=NULL)
{
  sizeType rows,cols;
  std::vector<I> r;
  std::vector<I> c;
  std::vector<T> v;
  readBinaryData(rows,is);
  readBinaryData(cols,is);
  readBinaryData(r,is);
  readBinaryData(c,is);
  readBinaryData(v,is);
  m.resize(rows,cols);
  m.reserve(v.size());
  for(sizeType ci=0; ci<m.cols(); ++ci) {
    m.startVec(ci);
    for(sizeType off=c[ci]; off<c[ci+1]; off++)
      m.insertBack(r[off],ci)=v[off];
  }
  m.finalize();
  return is.good();
}
template <typename T,int O,typename I>
bool writeBinaryData(const Eigen::SparseMatrix<T,O,I>& m,std::ostream& os,IOData* dat=NULL)
{
  std::vector<I> r(m.nonZeros());
  std::vector<I> c(m.cols()+1);
  std::vector<T> v(m.nonZeros());
  for(sizeType k=0,offr=0; k<m.outerSize(); ++k)
    for(typename Eigen::SparseMatrix<T,O,I>::InnerIterator it(m,k); it; ++it) {
      v[offr]=it.value();
      r[offr++]=it.row();
      c[k+1]++;
    }
  for(sizeType k=0; k<m.outerSize(); ++k)
    c[k+1]+=c[k];
  writeBinaryData(m.rows(),os);
  writeBinaryData(m.cols(),os);
  writeBinaryData(r,os);
  writeBinaryData(c,os);
  writeBinaryData(v,os);
  return os.good();
}
template <typename T,typename MT>
Eigen::SparseMatrix<T,0,sizeType> toSparse(const MT& m)
{
  Eigen::SparseMatrix<T,0,sizeType> ret;
  ParallelVector<Eigen::Triplet<T,sizeType> > trips;
  for(sizeType r=0; r<m.rows(); r++)
    for(sizeType c=0; c<m.cols(); c++)
      trips.push_back(Eigen::Triplet<T,sizeType>(r,c,m(r,c)));
  ret.resize(m.rows(),m.cols());
  ret.setFromTriplets(trips.begin(),trips.end());
  return ret;
}
template <typename T>
Eigen::SparseMatrix<T,0,sizeType> concat(const Eigen::SparseMatrix<T,0,sizeType>& A,const Eigen::SparseMatrix<T,0,sizeType>& B)
{
  ASSERT(A.cols()==B.cols())
  Eigen::SparseMatrix<T,0,sizeType> M(A.rows()+B.rows(),A.cols());
  M.reserve(A.nonZeros()+B.nonZeros());
  for(sizeType c=0; c<A.cols(); ++c) {
    M.startVec(c);
    for(typename Eigen::SparseMatrix<T,0,sizeType>::InnerIterator itA(A,c); itA; ++itA)
      M.insertBack(itA.row(),c)=itA.value();
    for(typename Eigen::SparseMatrix<T,0,sizeType>::InnerIterator itB(B,c); itB; ++itB)
      M.insertBack(itB.row()+A.rows(),c)=itB.value();
  }
  M.finalize();
  return M;
}
//sparseVec
template <typename Derived>
static void addBlock(Eigen::SparseVector<typename Derived::Scalar,0,sizeType>& H,sizeType r,const Eigen::MatrixBase<Derived>& coef)
{
  for(sizeType i=0; i<coef.rows(); i++)
    H.coeffRef(r+i)+=coef[i];
}
//svec indices
static sizeType IJToTri(sizeType N,sizeType r,sizeType c)
{
  if(r>c)std::swap(r,c);
  return N*(N-1)/2-(N-r)*(N-r-1)/2+c;
}
static void TriToIJ(sizeType N,sizeType k,sizeType& i,sizeType& j)
{
  i=N-1-sizeType(std::sqrt(-8*k+4*N*(N+1)-7)/2.0-0.5);
  j=k+i-N*(N+1)/2+(N-i)*(N+1-i)/2;
}
static sizeType NSVecToN(sizeType N)
{
  return std::sqrt(N*2);
}
static void debugTriToIJ(sizeType N)
{
  sizeType i,j,off=0;
  for(sizeType r=0; r<N; r++)
    for(sizeType c=r; c<N; c++,off++) {
      TriToIJ(N,off,i,j);
      ASSERT(i==r && j==c && IJToTri(N,i,j)==off)
    }
}
//svec
template <typename T>
static typename Eigen::Matrix<T,-1,1> toSVec(const Eigen::Matrix<T,-1,-1>& m)
{
  typedef Eigen::Matrix<T,-1,1> Vec;
  //typedef Eigen::Matrix<T,-1,-1> DMat;
  const T sqrt2=std::sqrt((T)2);
  Vec ret=Vec::Zero(m.rows()*(m.rows()+1)/2);
  for(sizeType r=0,off=0; r<m.rows(); r++)
    for(sizeType c=r; c<m.cols(); c++,off++)
      ret[off]=r==c?m(r,c):m(r,c)*sqrt2;
  return ret;
}
template <typename T>
static typename Eigen::Matrix<T,-1,-1> fromSVec(const Eigen::Matrix<T,-1,1>& s)
{
  //typedef Eigen::Matrix<T,-1,1> Vec;
  typedef Eigen::Matrix<T,-1,-1> DMat;
  const T sqrt2=std::sqrt((T)2);
  sizeType rows=NSVecToN(s.size());
  DMat ret=DMat::Zero(rows,rows);
  for(sizeType r=0,off=0; r<ret.rows(); r++)
    for(sizeType c=r; c<ret.cols(); c++,off++)
      if(r==c)
        ret(r,c)=s[off];
      else ret(r,c)=ret(c,r)=s[off]/sqrt2;
  return ret;
}
//visualize
template <typename S,int O,typename I>
static void writeSMatVTK(const typename Eigen::SparseMatrix<S,O,I>& coef,const std::string& path,const Coli* blkR,const Coli* blkC)
{
  VTKWriter<scalar> os("SMat",path,true);
  std::vector<Vec3,Eigen::aligned_allocator<Vec3>> vss;
  std::vector<Vec3i,Eigen::aligned_allocator<Vec3i>> tss;
  std::vector<Vec2i,Eigen::aligned_allocator<Vec2i>> lss;
  //element
  for(sizeType k=0; k<coef.outerSize(); ++k)
    for(typename Eigen::SparseMatrix<S,O,I>::InnerIterator it(coef,k); it; ++it) {
      sizeType off=(sizeType)vss.size();
      vss.push_back(Vec3(it.col()  ,coef.rows()-it.row()-1,1));
      vss.push_back(Vec3(it.col()+1,coef.rows()-it.row()-1,1));
      vss.push_back(Vec3(it.col()+1,coef.rows()-it.row()  ,1));
      vss.push_back(Vec3(it.col()  ,coef.rows()-it.row()  ,1));
      tss.push_back(Vec3i(off+0,off+1,off+2));
      tss.push_back(Vec3i(off+0,off+2,off+3));
    }
  //line
  {
    sizeType off=(sizeType)vss.size();
    vss.push_back(Vec3(0          ,0          ,1));
    vss.push_back(Vec3(coef.cols(),0          ,1));
    vss.push_back(Vec3(coef.cols(),coef.rows(),1));
    vss.push_back(Vec3(0          ,coef.rows(),1));
    lss.push_back(Vec2i(off+0,off+1));
    lss.push_back(Vec2i(off+1,off+2));
    lss.push_back(Vec2i(off+2,off+3));
    lss.push_back(Vec2i(off+3,off+0));
  }
  //blk lines
  if(blkR)
    for(sizeType i=0,Y=coef.rows(); i<blkR->size(); Y-=blkR->coeff(i),i++)
      if(Y>0 && Y<coef.rows()) {
        sizeType off=(sizeType)vss.size();
        vss.push_back(Vec3(0          ,Y,1));
        vss.push_back(Vec3(coef.cols(),Y,1));
        lss.push_back(Vec2i(off+0,off+1));
      }
  if(blkC)
    for(sizeType i=0,X=0; i<blkC->size(); X+=blkC->coeff(i),i++)
      if(X>0 && X<coef.cols()) {
        sizeType off=(sizeType)vss.size();
        vss.push_back(Vec3(X,0          ,1));
        vss.push_back(Vec3(X,coef.rows(),1));
        lss.push_back(Vec2i(off+0,off+1));
      }
  //feed
  os.appendPoints(vss.begin(),vss.end());
  os.appendCells(tss.begin(),tss.end(),VTKWriter<scalar>::TRIANGLE);
  os.appendCells(lss.begin(),lss.end(),VTKWriter<scalar>::LINE);
}

PRJ_END

#endif
