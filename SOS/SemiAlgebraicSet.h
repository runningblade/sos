#ifndef SEMI_ALGEBRAIC_SET_H
#define SEMI_ALGEBRAIC_SET_H

#include "SOSPolynomial.h"

PRJ_BEGIN

struct SemiAlgebraicSet
{
  typedef SOSPolynomial<scalarD,'x'> PolyX;
  typedef SOSPolynomial<PolyX,'a'> PolyXA;
  SemiAlgebraicSet():_nrEq(0) {}
  std::vector<PolyXA> _f;
  sizeType _nrEq;   //the first nrEq f are equality constraints, the rest are inequality constraints
};

PRJ_END

#endif
