#include "SDPAGMP.h"
#if defined(SDPA_GMP_SUPPORT)||defined(SDPA_DD_SUPPORT)||defined(SDPA_QD_SUPPORT)
#include <sdpa_io.h>

USE_PRJ_NAMESPACE
using namespace sdpa;

//Entry
SDPAGMP::Entry::Entry() {}
SDPAGMP::Entry::Entry(int bid,int row,int col,double val):_bid(bid),_row(row),_col(col),_val(val) {}
bool SDPAGMP::Entry::operator<(const Entry& other) const {
  return _bid<other._bid;
}
//SDPAGMP
SDPAGMP::SDPAGMP():_Display(NULL) {}
void SDPAGMP::setParameterType(ParameterType type)
{
  if(type==PARAMETER_DEFAULT) {
    _param.setDefaultParameter(Parameter::PARAMETER_DEFAULT);
  } else if(type==PARAMETER_UNSTABLE_BUT_FAST) {
    _param.setDefaultParameter(Parameter::PARAMETER_UNSTABLE_BUT_FAST);
  } else if(type==PARAMETER_STABLE_BUT_SLOW) {
    _param.setDefaultParameter(Parameter::PARAMETER_STABLE_BUT_SLOW);
  }
}
void SDPAGMP::setParameterMaxIteration(int maxIteration)
{
  _param.maxIteration=maxIteration;
}
void SDPAGMP::initializeUpperTriangleSpace()
{
  _varSDP.resize(m()+1);
  _varLP.resize(m()+1);
}
void SDPAGMP::initializeUpperTriangle()
{
  reorderCones();
  int nSDP=nSDPCone();
  std::vector<int> SDP_sp_index(nSDP);
  std::vector<int> SDP_sp_blockStruct(nSDP);
  std::vector<int> SDP_sp_NonZeroNumber(nSDP);
  int nLP=nLPCone();
  std::vector<int> LP_sp_index(nLP);
  for(sizeType k=0; k<=m(); k++) {
    sizeType previous_index;
    sizeType index;
    //SDP
    previous_index=-1;
    int SDP_sp_nBlock=0;
    std::sort(_varSDP[k].begin(),_varSDP[k].end());
    for(sizeType i=0; i<(sizeType)_varSDP[k].size(); i++) {
      index=_varSDP[k][i]._bid;
      if(previous_index!=index) {
        SDP_sp_index[SDP_sp_nBlock]=_reorderIndex[index];
        SDP_sp_blockStruct[SDP_sp_nBlock]=_blockStruct[index];
        SDP_sp_NonZeroNumber[SDP_sp_nBlock]=1;
        previous_index=index;
        SDP_sp_nBlock++;
      } else {
        SDP_sp_NonZeroNumber[SDP_sp_nBlock-1]++;
      }
    }
    //LP
    previous_index=-1;
    int LP_sp_nBlock=0;
    std::sort(_varLP[k].begin(),_varLP[k].end());
    for(sizeType i=0; i<(sizeType)_varLP[k].size(); i++) {
      index=_varLP[k][i]._bid;
      if(previous_index!=index) {
        LP_sp_index[LP_sp_nBlock]=_reorderIndex[index];
        previous_index=index;
        LP_sp_nBlock++;
      } else {
        ASSERT_MSG(false,"Duplicate index in LP not allowed!")
      }
    }
    ASSERT(SDP_sp_nBlock<=nSDP && LP_sp_nBlock<=nLP)
    //initialize
    if (k==0)
      _inputData.C.initialize(SDP_sp_nBlock,&SDP_sp_index[0],&SDP_sp_blockStruct[0],&SDP_sp_NonZeroNumber[0],0,NULL,NULL,NULL,LP_sp_nBlock,&LP_sp_index[0]);
    else
      _inputData.A[k-1].initialize(SDP_sp_nBlock,&SDP_sp_index[0],&SDP_sp_blockStruct[0],&SDP_sp_NonZeroNumber[0],0,NULL,NULL,NULL,LP_sp_nBlock,&LP_sp_index[0]);
  }
}
void SDPAGMP::initializeSolve()
{
  fillSDPCone();
  fillLPCone();
  //init index
  _inputData.initialize_index(nSDPCone(),0,nLPCone(),_com);
  //if possible , change C and A to Dense
  //_inputData.C.changeToDense();
  //for(int i=0; i<m(); ++i)
  //  _inputData.A[i].changeToDense();
}
void SDPAGMP::fillSDPCone()
{
  for(sizeType k=0; k<=m(); k++)
    for(sizeType i=0; i<(sizeType)_varSDP[k].size(); i++) {
      const Entry& e=_varSDP[k][i];
      if(k==0)
        _inputData.C.setElement_SDP(_reorderIndex[e._bid],e._col,e._row,-e._val);
      else _inputData.A[k-1].setElement_SDP(_reorderIndex[e._bid],e._col,e._row,e._val);
    }
}
void SDPAGMP::fillLPCone()
{
  for(sizeType k=0; k<=m(); k++)
    for(sizeType i=0; i<(sizeType)_varLP[k].size(); i++) {
      const Entry& e=_varLP[k][i];
      if(k==0)
        _inputData.C.setElement_LP(_reorderIndex[e._bid],-e._val);
      else _inputData.A[k-1].setElement_LP(_reorderIndex[e._bid],e._val);
    }
}

void SDPAGMP::solve(sdpa::Solutions* ptInit)
{
  TimeStart(TOTAL_TIME_START1);
  static const double KAPPA=1.2;
  int m=_inputData.b.nDim;
  int nSDP=nSDPCone(),nLP=nLPCone();
  FILE* fpOut=fopen("OutputSDP.txt","w");
  Newton newton(m,nSDP,&_blockStructSDP[0],0,NULL,nLP);
  // 2008/03/12 kazuhide nakata
  Chordal chordal;
  // rMessage("ordering bMat: start");
  chordal.ordering_bMat(m,(sizeType)_blockStruct.size(),_inputData,fpOut);
  // rMessage("ordering bMat: end");
  newton.initialize_bMat(m,chordal,_inputData,fpOut);
  chordal.terminate();
  // rMessage("newton.computeFormula_SDP: start");
  newton.computeFormula_SDP(_inputData,0.0,KAPPA);
  // rMessage("newton.computeFormula_SDP: end");
  // set initial solutions.
  WorkVariables work;
  DenseLinearSpace initPt_xMat;
  DenseLinearSpace initPt_zMat;
  _currentPt.initialize(m,nSDP,&_blockStructSDP[0],0,NULL,nLP,_param.lambdaStar,_com);
  work.initialize(m,nSDP,&_blockStructSDP[0],0,NULL,nLP);
  if(ptInit) {
    ComputeTime com;
    _currentPt.yVec.copyFrom(ptInit->yVec);
    _currentPt.xMat.copyFrom(ptInit->xMat);
    _currentPt.zMat.copyFrom(ptInit->zMat);
    _currentPt.computeInverse(work,com);
    initPt_xMat.copyFrom(_currentPt.xMat);
    initPt_zMat.copyFrom(_currentPt.zMat);
  }
  Residuals initRes(m,nSDP,&_blockStructSDP[0],0,NULL,nLP,_inputData,_currentPt);
  Residuals currentRes;
  currentRes.copyFrom(initRes);
  // rMessage("initial currentRes = ");
  // currentRes.display(Display);
  StepLength alpha;
  DirectionParameter beta(_param.betaStar);
  Switch reduction(Switch::ON);
  AverageComplementarity mu(_param.lambdaStar);
  if(ptInit)
    mu.initialize(_currentPt);
  // rMessage("init mu"); mu.display();
  RatioInitResCurrentRes theta(_param,initRes);
  SolveInfo solveInfo(_inputData,_currentPt,mu.initial,_param.omegaStar);
  Phase phase(initRes,solveInfo,_param,_currentPt.nDim);
  int pIteration = 0;
  IO::printHeader(fpOut,_Display);
  // -----------------------------------------------------
  // Here is MAINLOOP
  // -----------------------------------------------------
  TimeStart(MAIN_LOOP_START1);
  // explicit maxIteration
  // param.maxIteration = 2;
  while (phase.updateCheck(currentRes, solveInfo, _param)
         && pIteration < _param.maxIteration) {
    // rMessage(" turn hajimari " << pIteration );
    // Mehrotra's Predictor
    TimeStart(MEHROTRA_PREDICTOR_START1);
    // set variable of Mehrotra
    reduction.MehrotraPredictor(phase);
    beta.MehrotraPredictor(phase, reduction, _param);
    // rMessage("reduction = "); reduction.display();
    // rMessage("phase = "); phase.display();
    // rMessage("beta.predictor.value = " << beta.value);
    // rMessage(" mu = " << mu.current);
    // rMessage("currentPt = "); currentPt.display();
    bool isSuccessCholesky;
    isSuccessCholesky = newton.Mehrotra(Newton::PREDICTOR,
                                        _inputData, _currentPt,
                                        currentRes,
                                        mu, beta, reduction,
                                        phase,work,_com);
    if (isSuccessCholesky == false) {
      break;
    }
    // rMessage("newton predictor = "); newton.display();
    TimeEnd(MEHROTRA_PREDICTOR_END1);
    _com.Predictor += TimeCal(MEHROTRA_PREDICTOR_START1,
                              MEHROTRA_PREDICTOR_END1);
    TimeStart(STEP_PRE_START1);
    alpha.MehrotraPredictor(_inputData, _currentPt, phase, newton,
                            work, _com);
    // rMessage("alpha predictor = "); alpha.display();
    TimeStart(STEP_PRE_END1);
    _com.StepPredictor += TimeCal(STEP_PRE_START1,STEP_PRE_END1);
    // rMessage("alphaStar = " << param.alphaStar);
    // Mehrotra's Corrector
    // rMessage(" Corrector ");
    TimeStart(CORRECTOR_START1);
    beta.MehrotraCorrector(phase,alpha,_currentPt, newton,mu,_param);
    // rMessage("beta corrector = " << beta.value);
#if 1 // 2007/08/29 kazuhide nakata
    // add stopping criteria: objValPrimal < ObjValDual
    //	if ((pIteration > 10) &&
    if ((phase.value == SolveInfo::pdFEAS) &&
        ((beta.value > 5)||(solveInfo.objValPrimal < solveInfo.objValDual))) {
      break;
    }
#endif
    newton.Mehrotra(Newton::CORRECTOR,
                    _inputData, _currentPt, currentRes,
                    mu, beta, reduction, phase,work,_com);
    // rMessage("currentPt = "); currentPt.display();
    // rMessage("newton corrector = "); newton.display();
    TimeEnd(CORRECTOR_END1);
    _com.Corrector += TimeCal(CORRECTOR_START1,
                              CORRECTOR_END1);
    TimeStart(CORRECTOR_STEP_START1);
    alpha.MehrotraCorrector(_inputData, _currentPt, phase,
                            reduction, newton, mu, theta,
                            work, _param, _com);
    // rMessage("alpha corrector = "); alpha.display();
    TimeEnd(CORRECTOR_STEP_END1);
    _com.StepCorrector += TimeCal(CORRECTOR_STEP_START1,
                                  CORRECTOR_STEP_END1);
    // the end of Corrector
    IO::printOneIteration(pIteration, mu, theta, solveInfo,
                          alpha, beta, fpOut, _Display);
    if (_currentPt.update(alpha,newton,work,_com)==false) {
      // if step length is too short,
      // we finish algorithm
      rMessage("cannot move");
      //   memo by kazuhide nakata
      //   StepLength::MehrotraCorrector
      //   thetaMax*mu.initial -> thetamax*thetaMax*mu.initial
      break;
    }
    // rMessage("currentPt = "); currentPt.display();
    // rMessage("updated");
    theta.update(reduction,alpha);
    mu.update(_currentPt);
    currentRes.update(m,_inputData, _currentPt, _com);
    theta.update_exact(initRes,currentRes);
    if(ptInit) {
      solveInfo.update(_inputData, initPt_xMat, initPt_zMat, _currentPt,
                       currentRes, mu, theta, _param);
    } else {
      solveInfo.update(_param.lambdaStar,_inputData, _currentPt,
                       currentRes, mu, theta, _param);
    }
    // 2007/09/18 kazuhide nakata
    // print information of ObjVal, residual, gap, complementarity
    //	solveInfo.check(inputData, currentPt, currentRes, mu, theta, param);
    pIteration++;
    fflush(_Display);
  } // end of MAIN_LOOP
  TimeEnd(MAIN_LOOP_END1);
  _com.MainLoop = TimeCal(MAIN_LOOP_START1, MAIN_LOOP_END1);
  currentRes.compute(m,_inputData,_currentPt);
  TimeEnd(TOTAL_TIME_END1);
  _com.TotalTime = TimeCal(TOTAL_TIME_START1, TOTAL_TIME_END1);
#if REVERSE_PRIMAL_DUAL
  phase.reverse();
#endif
  //IO::printLastInfo(pIteration, mu, theta, solveInfo, alpha, beta,
  //                  currentRes, phase, _currentPt, _com.TotalTime,
  //                  _inputData, work, _com, _param, fpOut, Display);
  // _com.display(fpOut);
  fclose(fpOut);
  _retCode=phase.value;
}
void SDPAGMP::setDisplay(FILE* Display)
{
  _Display=Display;
}
SDPAGMP::phaseType SDPAGMP::getPhaseValue() const
{
  if(_retCode==SolveInfo::noINFO)
    return noINFO;
  if(_retCode==SolveInfo::pFEAS)
    return pFEAS;
  if(_retCode==SolveInfo::dFEAS)
    return dFEAS;
  if(_retCode==SolveInfo::pdFEAS)
    return pdFEAS;
  if(_retCode==SolveInfo::pdINF)
    return pdINF;
  if(_retCode==SolveInfo::pFEAS_dINF)
    return pFEAS_dINF;
  if(_retCode==SolveInfo::pINF_dFEAS)
    return pINF_dFEAS;
  if(_retCode==SolveInfo::pdOPT)
    return pdOPT;
  if(_retCode==SolveInfo::pUNBD)
    return pUNBD;
  if(_retCode==SolveInfo::dUNBD)
    return dUNBD;
  return noINFO;
}

void SDPAGMP::inputConstraintNumber(int m)
{
  _inputData=InputData();
  _inputData.initialize_bVec(m);
  _inputData.A=new sdpa::SparseLinearSpace[m];
  Eigen::Map<Vec>(_inputData.b.ele,_inputData.b.nDim).setConstant(SDPA_FLOAT(0));
}
void SDPAGMP::inputBlockNumber(int nBlock)
{
  _blockStruct.resize(nBlock);
}
void SDPAGMP::inputBlockSize(int l,int size)
{
  ASSERT(l>=1 && l<=(sizeType)_blockStruct.size())
  _blockStruct[l-1]=size;
}
void SDPAGMP::inputBlockType(int l,ConeType coneType)
{
  ASSERT(l>=1 && l<=(sizeType)_blockStruct.size())
  ASSERT(coneType!=SOCP)
  if(coneType==SDP) {
    ASSERT(_blockStruct[l-1]>=1)
  } else if(coneType==LP) {
    ASSERT(_blockStruct[l-1]==1)
  }
}
void SDPAGMP::inputCVec(int k,double value)
{
  ASSERT(k>=1 && k<=m())
  _inputData.b.ele[k-1]=SDPA_FLOAT(value);
}
void SDPAGMP::inputElement(int k,int l,int i,int j,double value,bool inputCheck)
{
  l--;
  i--;
  j--;
  if(inputCheck) {
    ASSERT(k>=0 && k<=m())
    ASSERT(l>=0 && l<nBlock())
    ASSERT(i>=0 && i<_blockStruct[l])
    ASSERT(j>=0 && j<_blockStruct[l] && j>=i)
    ASSERT(_blockStruct[l]>1 || (i==j && i==0))
  }
  if(_blockStruct[l]==1)
    _varLP[k].push_back(Entry(l,i,j,value));
  else _varSDP[k].push_back(Entry(l,i,j,value));
}

const SDPAGMP::SDPA_FLOAT* SDPAGMP::getResultXVec() const
{
  return _currentPt.yVec.ele;
}
double SDPAGMP::get_d(const SDPA_FLOAT& f) const
{
#if defined(SDPA_GMP_SUPPORT)
  return f.get_d();
#else
  return f.x[0];
#endif
}
void SDPAGMP::reorderCones()
{
  sizeType nSDP=0,nLP=0;
  _blockStructSDP.clear();
  _reorderIndex.resize(_blockStruct.size());
  for(sizeType i=0; i<nBlock(); i++) {
    if(_blockStruct[i]==1) {
      _reorderIndex[i]=nLP;
      nLP++;
    } else {
      _reorderIndex[i]=nSDP;
      _blockStructSDP.push_back(_blockStruct[i]);
      nSDP++;
    }
  }
}
int SDPAGMP::nSDPCone() const
{
  sizeType nSDP=0;
  for(sizeType i=0; i<nBlock(); i++)
    if(_blockStruct[i]>1)
      nSDP++;
  return nSDP;
}
int SDPAGMP::nLPCone() const
{
  sizeType nLP=0;
  for(sizeType i=0; i<nBlock(); i++)
    if(_blockStruct[i]==1)
      nLP++;
  return nLP;
}
int SDPAGMP::nBlock() const
{
  return (int)_blockStruct.size();
}
int SDPAGMP::m() const
{
  return _inputData.b.nDim;
}
#endif
