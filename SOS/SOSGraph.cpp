#include "SOSGraph.h"
#ifdef METIS_SUPPORT
#include "metis.h"
#endif

USE_PRJ_NAMESPACE

//Graph
SOSGraph::SOSGraph(const PolyXA& poly,const SemiAlgebraicSet* K,ORDER type)
{
  compute(poly,K,type);
}
SOSGraph::SOSGraph(const Matd& csp,ORDER type)
{
  compute(csp,type);
}
bool SOSGraph::checkRIPMaximalCliques() const
{
  std::set<sizeType> unionClique;
  for(sizeType i=0; i<(sizeType)_cliques.size(); i++) {
    std::vector<sizeType> KInter;
    std::set_intersection(_cliques[i].begin(),_cliques[i].end(),unionClique.begin(),unionClique.end(),std::back_inserter(KInter));
    //check whether some clique contain KInter
    bool contain=i==0;
    for(sizeType j=0; j<i; j++) {
      std::vector<sizeType> diff;
      std::set_difference(KInter.begin(),KInter.end(),_cliques[j].begin(),_cliques[j].end(),std::back_inserter(diff));
      if(diff.empty()) {
        contain=true;
        break;
      }
    }
    if(!contain)
      return false;
    unionClique.insert(_cliques[i].begin(),_cliques[i].end());
  }
  return true;
}
sizeType SOSGraph::getCliqueIdFirst(const PolyXA& poly) const
{
  std::set<sizeType> ids;
  for(sizeType i=0; i<(sizeType)poly._terms.size(); i++)
    ids.insert(poly._terms[i]._id.begin(),poly._terms[i]._id.end());
  for(sizeType i=0; i<(sizeType)_cliques.size(); i++) {
    std::vector<sizeType> diff;
    std::set_difference(ids.begin(),ids.end(),_cliques[i].begin(),_cliques[i].end(),std::back_inserter(diff));
    if(diff.empty())
      return i;
  }
  ASSERT_MSGV(false,"Cannot function clique that contain function f: %s!",poly.toString().c_str())
  return -1;
}
std::vector<sizeType> SOSGraph::getCliqueIdAll(const PolyXA& poly) const
{
  std::vector<sizeType> ret;
  std::set<sizeType> ids;
  for(sizeType i=0; i<(sizeType)poly._terms.size(); i++)
    ids.insert(poly._terms[i]._id.begin(),poly._terms[i]._id.end());
  for(sizeType i=0; i<(sizeType)_cliques.size(); i++) {
    std::vector<sizeType> diff;
    std::set_difference(ids.begin(),ids.end(),_cliques[i].begin(),_cliques[i].end(),std::back_inserter(diff));
    if(diff.empty())
      ret.push_back(i);
  }
  ASSERT_MSGV(!ret.empty(),"Cannot function clique that contain function f: %s!",poly.toString().c_str())
  return ret;
}
const std::set<sizeType>& SOSGraph::getClique(const PolyXA& poly) const
{
  return _cliques[getCliqueIdFirst(poly)];
}
const std::vector<std::set<sizeType>>& SOSGraph::getMaximalCliques() const
{
  return _cliques;
}
void SOSGraph::writeVTK(const std::string& csp,const std::string& cspE) const
{
  writeSMatVTK(toSparse(_csp),csp,NULL,NULL);
  writeSMatVTK(toSparse(_cspE),cspE,NULL,NULL);
}
//helper
SOSGraph::SMat SOSGraph::toSparse(const Matd& p) const
{
  SMat ret;
  STrips trips;
  for(sizeType r=0; r<p.rows(); r++)
    for(sizeType c=0; c<p.cols(); c++)
      if(p(r,c)>0)
        trips.push_back(STrip(r,c,1));
  ret.resize(p.rows(),p.cols());
  ret.setFromTriplets(trips.begin(),trips.end());
  return ret;
}
std::vector<sizeType> SOSGraph::fillReducing() const
{
#ifdef METIS_SUPPORT
  //METIS_API(int) METIS_NodeND
  //(idx_t *nvtxs, idx_t *xadj, idx_t *adjncy, idx_t *vwgt,
  // idx_t *options, idx_t *perm, idx_t *iperm);
  idx_t nvtxs=_csp.rows();
  std::vector<idx_t> xadj(1,0),adjncy;
  Eigen::Matrix<idx_t,-1,1> perm(nvtxs),iperm(nvtxs);
  for(sizeType r=0; r<_csp.rows(); r++) {
    for(sizeType c=0; c<_csp.cols(); c++)
      if(_csp(r,c)==1 && c!=r)
        adjncy.push_back(c);
    xadj.push_back((sizeType)adjncy.size());
  }
  int code=METIS_NodeND(&nvtxs,&xadj[0],&adjncy[0],NULL,NULL,perm.data(),iperm.data());
  ASSERT_MSG(code==METIS_OK,"Fill-in reducing ordering failed from Metis!")
  //return
  std::vector<sizeType> ret(nvtxs);
  Eigen::Map<Coli>(&ret[0],nvtxs)=perm.cast<sizeType>();
  return ret;
#else
  FUNCTION_NOT_IMPLEMENTED
  return std::vector<sizeType>();
#endif
}
Matd SOSGraph::extend(const std::vector<sizeType>& order,std::vector<std::set<sizeType>>& cliques) const
{
  ASSERT((sizeType)order.size()==_csp.rows())
  //compute cspE
  Matd cspE=_csp;
  while(true) {
    bool changed=false;
    for(sizeType i=0; i<(sizeType)order.size(); i++) {
      //find v+adj(v)
      std::set<sizeType> v_adjv;
      for(sizeType j=0; j<=i; j++)
        if(cspE(order[i],order[j])>0)
          v_adjv.insert(order[j]);
      cliques.push_back(v_adjv);
      //make v+adj(v) complete
      for(const std::set<sizeType>::value_type &r:v_adjv) {
        for(const std::set<sizeType>::value_type &c:v_adjv) {
          if(cspE(r,c)==0) {
            changed=true;
            cspE(r,c)=cspE(c,r)=1;
          }
        }
      }
    }
    if(!changed)
      break;
  }
  //compute maximal cliques
  cliques.clear();
  for(sizeType i=0; i<(sizeType)order.size(); i++) {
    //find v+adj(v)
    std::set<sizeType> v_adjv;
    for(sizeType j=0; j<=i; j++)
      if(cspE(order[i],order[j])>0)
        v_adjv.insert(order[j]);
    cliques.push_back(v_adjv);
    //make sure this is maximal
    while(!cliques.empty()) {
      std::vector<sizeType> diff;
      std::set_difference(cliques.back().begin(),cliques.back().end(),v_adjv.begin(),v_adjv.end(),std::back_inserter(diff));
      if(diff.empty())  //cliques.back()-v_adjv is not empty, this means that we can keep cliques.back and continue
        cliques.pop_back();
      else break;
    }
    cliques.push_back(v_adjv);
  }
  return cspE;
}
void SOSGraph::addF2Csp(Matd& csp,const PolyXA& poly) const
{
  std::set<sizeType> ids;
  for(sizeType i=0; i<(sizeType)poly._terms.size(); i++)
    ids.insert(poly._terms[i]._id.begin(),poly._terms[i]._id.end());
  for(const std::set<sizeType>::value_type &r:ids) {
    for(const std::set<sizeType>::value_type &c:ids) {
      csp(r,c)=csp(c,r)=1;
    }
  }
}
void SOSGraph::compute(const PolyXA& poly,const SemiAlgebraicSet* K,ORDER type)
{
  //nrVarA
  sizeType nrVar=poly.nrVar<'a'>();
  if(K)
    for(sizeType i=0; i<(sizeType)K->_f.size(); i++)
      nrVar=std::max<sizeType>(nrVar,K->_f[i].nrVar<'a'>());
  //build csp[poly]
  Matd csp=Matd::Identity(nrVar,nrVar);
  for(sizeType i=0; i<(sizeType)poly._terms.size(); i++) {
    sizeType nrVarTerm=(sizeType)poly._terms[i]._id.size();
    for(sizeType r=0; r<nrVarTerm; r++)
      for(sizeType c=0; c<nrVarTerm; c++) {
        csp(poly._terms[i]._id[r],poly._terms[i]._id[c])=1;
        csp(poly._terms[i]._id[c],poly._terms[i]._id[r])=1;
      }
  }
  if(K) {
    //build csp[K]
    for(sizeType i=0; i<(sizeType)K->_f.size(); i++)
      addF2Csp(csp,K->_f[i]);
  }
  compute(csp,type);
}
void SOSGraph::compute(const Matd& csp,ORDER type)
{
  ASSERT(csp.rows()==csp.cols())
  _csp=(csp+csp.transpose())/2;
  _csp.diagonal().setOnes();
  std::vector<sizeType> order;
  if(type==METIS)
    order=fillReducing();
  else if(type==METIS_REVERSED) {
    order=fillReducing();
    std::reverse(order.begin(),order.end());
  } else if(type==NATURAL) {
    sizeType n=0;
    order.resize(csp.rows());
    std::generate(order.begin(),order.end(),[&]() {
      return n++;
    });
  } else {
    ASSERT_MSG(false,"Unsupported order type!")
  }
  _cspE=extend(order,_cliques);
  ASSERT(checkRIPMaximalCliques())
}
