#include "SparseUtils.h"

PRJ_BEGIN

//#define CHECK_SCALE
//maxAbs
scalarD absMax(const Eigen::SparseMatrix<scalarD,0,sizeType>& h)
{
  scalarD ret=0;
  for(sizeType k=0; k<h.outerSize(); ++k)
    for(typename Eigen::SparseMatrix<scalarD,0,sizeType>::InnerIterator it(h,k); it; ++it)
      ret=std::max(ret,std::abs(it.value()));
  return ret;
}
scalarD absMaxRel(const Eigen::SparseMatrix<scalarD,0,sizeType>& h,const Eigen::SparseMatrix<scalarD,0,sizeType>& hRef,bool detail)
{
  sizeType row=-1,col=-1;
  scalarD ret=0,num=0,denom=0;
  //check against h
  for(sizeType k=0; k<h.outerSize(); ++k)
    for(typename Eigen::SparseMatrix<scalarD,0,sizeType>::InnerIterator it(h,k); it; ++it) {
      scalarD err=std::abs(it.value()-hRef.coeff(it.row(),it.col()));
      scalarD ref=std::abs(hRef.coeff(it.row(),it.col()));
      scalarD rel=err/std::max<scalarD>(ref,1E-6f);
      if(err<1E-6f)
        continue;
      if(rel>ret) {
        num=err;
        denom=ref;
        row=it.row();
        col=it.col();
      }
      ret=std::max(ret,rel);
    }
  //check against hRef
  for(sizeType k=0; k<hRef.outerSize(); ++k)
    for(typename Eigen::SparseMatrix<scalarD,0,sizeType>::InnerIterator it(hRef,k); it; ++it) {
      scalarD err=std::abs(h.coeff(it.row(),it.col())-it.value());
      scalarD ref=std::abs(it.value());
      scalarD rel=err/std::max<scalarD>(ref,1E-6f);
      if(err<1E-6f)
        continue;
      if(rel>ret) {
        num=err;
        denom=ref;
        row=it.row();
        col=it.col();
      }
      ret=std::max(ret,rel);
    }
  if(detail) {
    INFOV("(%d,%d) Num: %f Denom: %f",row,col,num,denom)
  }
  return ret;
}
//build KKT matrix
Matd buildKKT(const Matd& h,const Matd& a,scalarD shift)
{
  Matd kkt;
  kkt.setZero(h.rows()+a.rows(),h.rows()+a.rows());
  kkt.block(0,0,h.rows(),h.rows())=h;
  kkt.block(0,0,h.rows(),h.rows()).diagonal().array()+=shift;
  kkt.block(h.rows(),0,a.rows(),a.cols())=a;
  kkt.block(0,h.rows(),a.cols(),a.rows())=a.transpose();
  return kkt;
}
Eigen::SparseMatrix<scalarD,0,sizeType> buildKKT(const Eigen::SparseMatrix<scalarD,0,sizeType>& h,const Eigen::SparseMatrix<scalarD,0,sizeType>& a,scalarD shift)
{
  typedef Eigen::SparseMatrix<scalarD,0,sizeType> SMat;
  SMat kkt;
  ParallelVector<Eigen::Triplet<scalarD,sizeType>> trips;
  kkt.resize(h.rows()+a.rows(),h.rows()+a.rows());
  for(sizeType k=0; k<h.outerSize(); ++k)
    for(SMat::InnerIterator it(h,k); it; ++it)
      trips.push_back(Eigen::Triplet<scalarD,sizeType>(it.row(),it.col(),it.value()));
  if(shift!=0)
    for(sizeType k=0; k<h.rows(); ++k)
      trips.push_back(Eigen::Triplet<scalarD,sizeType>(k,k,shift));
  for(sizeType k=0; k<a.outerSize(); ++k)
    for(SMat::InnerIterator it(a,k); it; ++it) {
      trips.push_back(Eigen::Triplet<scalarD,sizeType>(h.rows()+it.row(),it.col(),it.value()));
      trips.push_back(Eigen::Triplet<scalarD,sizeType>(it.col(),h.rows()+it.row(),it.value()));
    }
  kkt.setFromTriplets(trips.begin(),trips.end());
  return kkt;
}
//kronecker-product
Eigen::SparseMatrix<scalarD,0,sizeType> kronecker(const Eigen::SparseMatrix<scalarD,0,sizeType>& h,sizeType n)
{
  ParallelVector<Eigen::Triplet<scalarD,sizeType>> trips;
  for(sizeType k=0; k<h.outerSize(); ++k)
    for(typename Eigen::SparseMatrix<scalarD,0,sizeType>::InnerIterator it(h,k); it; ++it)
      for(sizeType d=0; d<n; d++)
        trips.push_back(Eigen::Triplet<scalarD,sizeType>(it.row()*n+d,it.col()*n+d,it.value()));

  Eigen::SparseMatrix<scalarD,0,sizeType> ret;
  ret.resize(h.rows()*n,h.cols()*n);
  ret.setFromTriplets(trips.begin(),trips.end());
  return ret;
}
//concat-diag
Eigen::SparseMatrix<scalarD,0,sizeType> concatDiag(const Eigen::SparseMatrix<scalarD,0,sizeType>& a,const Eigen::SparseMatrix<scalarD,0,sizeType>& b)
{
  ParallelVector<Eigen::Triplet<scalarD,sizeType>> trips;
  for(sizeType k=0; k<a.outerSize(); ++k)
    for(typename Eigen::SparseMatrix<scalarD,0,sizeType>::InnerIterator it(a,k); it; ++it)
      trips.push_back(Eigen::Triplet<scalarD,sizeType>(it.row(),it.col(),it.value()));
  for(sizeType k=0; k<b.outerSize(); ++k)
    for(typename Eigen::SparseMatrix<scalarD,0,sizeType>::InnerIterator it(b,k); it; ++it)
      trips.push_back(Eigen::Triplet<scalarD,sizeType>(a.rows()+it.row(),a.cols()+it.col(),it.value()));

  Eigen::SparseMatrix<scalarD,0,sizeType> ret;
  ret.resize(a.rows()+b.rows(),a.cols()+b.cols());
  ret.setFromTriplets(trips.begin(),trips.end());
  return ret;
}

PRJ_END
