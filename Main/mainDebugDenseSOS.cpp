#include "SOS/SOSProblem.h"
#include <CommonFile/Timing.h>

USE_PRJ_NAMESPACE

void comparePutBSOS(const std::string& name,SOSProblem::PolyXA poly,SemiAlgebraicSet K,sizeType d,sizeType k)
{
  poly-=SOSProblem::PolyXA("x0");
  {
    TBEG("DensePut"+name);
    SOSProblem prob;
    prob.outputWhenSolve(true);
    //prob.solverType(SOSProblem::SOLVER_MOSEK);
    prob.solverType(SOSProblem::SOLVER_SCS);
    prob.visualizeCSP("cspPutSOS"+name);
    prob.makePutSOS("SOS",name,poly,&K,k);
    prob.makeObjective(name,SOSProblem::PolyXA("-1*x0"));
    ASSERT(prob.solve())
    TEND();
    INFOV("DensePut: %f!",prob.getSolutionOfPolyVar(name,0))
  }
  {
    TBEG("DenseBSOS"+name);
    SOSProblem prob;
    prob.outputWhenSolve(true);
    //prob.solverType(SOSProblem::SOLVER_MOSEK);
    prob.solverType(SOSProblem::SOLVER_SCS);
    prob.visualizeCSP("cspBSOS"+name);
    prob.makeBSOS("SOS",name,poly,K,d,k);
    prob.makeObjective(name,SOSProblem::PolyXA("-1*x0"));
    ASSERT(prob.solve())
    TEND();
    INFOV("DenseBSOS: %f!",prob.getSolutionOfPolyVar(name,0))
  }
}
void exampleBSOS1()
{
  SOSProblem::PolyXA f("a1^2 -a2^2 +a3^2 -a4^2 +a1 -a2");
  SemiAlgebraicSet K;
  K._nrEq=0;
  K._f.push_back(SOSProblem::PolyXA("2*a1^2 +3*a2^2 +2*a1*a2 +2*a3^2 +3*a4^2 +2*a3*a4"));
  K._f.push_back(ScalarOfT<SOSProblem::PolyXA>::convert(1)-K._f.back());
  K._f.push_back(SOSProblem::PolyXA("3*a1^2 +2*a2^2 -4*a1*a2 +3*a3^2 +2*a4^2 -4*a3*a4"));
  K._f.push_back(ScalarOfT<SOSProblem::PolyXA>::convert(1)-K._f.back());
  K._f.push_back(SOSProblem::PolyXA("  a1^2 +6*a2^2 -4*a1*a2 +  a3^2 +6*a4^2 -4*a3*a4"));
  K._f.push_back(ScalarOfT<SOSProblem::PolyXA>::convert(1)-K._f.back());
  K._f.push_back(SOSProblem::PolyXA("  a1^2 +4*a2^2 -3*a1*a2 +  a3^2 +4*a4^2 -3*a3*a4"));
  K._f.push_back(ScalarOfT<SOSProblem::PolyXA>::convert(1)-K._f.back());
  K._f.push_back(SOSProblem::PolyXA("2*a1^2 +5*a2^2 +3*a1*a2 +2*a3^2 +5*a4^2 +3*a3*a4"));
  K._f.push_back(ScalarOfT<SOSProblem::PolyXA>::convert(1)-K._f.back());
  K._f.push_back(SOSProblem::PolyXA("a1"));
  K._f.push_back(SOSProblem::PolyXA("a2"));
  K._f.push_back(SOSProblem::PolyXA("a3"));
  K._f.push_back(SOSProblem::PolyXA("a4"));
  comparePutBSOS("p",f,K,1,1);
}
void exampleBSOS2()
{
  SOSProblem::PolyXA f("a1^4*a2^2 +a1^2*a2^4 -a1^2*a2^2");
  SemiAlgebraicSet K;
  K._nrEq=0;
  K._f.push_back(SOSProblem::PolyXA("  a1^2 +a2^2"));
  K._f.push_back(ScalarOfT<SOSProblem::PolyXA>::convert(1)-K._f.back());
  K._f.push_back(SOSProblem::PolyXA("3*a1^2 +2*a2^2 -4*a1*a2"));
  K._f.push_back(ScalarOfT<SOSProblem::PolyXA>::convert(1)-K._f.back());
  K._f.push_back(SOSProblem::PolyXA("  a1^2 +6*a2^4 -8*a1*a2 +2.5"));
  K._f.push_back(ScalarOfT<SOSProblem::PolyXA>::convert(1)-K._f.back());
  K._f.push_back(SOSProblem::PolyXA("  a1^4 +3*a2^4"));
  K._f.push_back(ScalarOfT<SOSProblem::PolyXA>::convert(1)-K._f.back());
  K._f.push_back(SOSProblem::PolyXA("  a1^2 +  a2^3"));
  K._f.push_back(ScalarOfT<SOSProblem::PolyXA>::convert(1)-K._f.back());
  K._f.push_back(SOSProblem::PolyXA("a1"));
  K._f.push_back(SOSProblem::PolyXA("a2"));
  comparePutBSOS("p",f,K,4,4);
}
int main()
{
  exampleBSOS1();
  exampleBSOS2();
  return 0;
}
