#include "SOS/SOSGraph.h"

USE_PRJ_NAMESPACE

int main()
{
#define N 100
  Matd ret=Matd::Zero(N,N);
  ret.diagonal().setOnes();
  for(sizeType i=0; i<ret.size()/20; i++) {
    sizeType r=RandEngine::randI(0,ret.rows()-1);
    sizeType c=RandEngine::randI(0,ret.rows()-1);
    ret(r,c)=ret(c,r)=1;
  }
  {
    SOSGraph graph(ret,SOSGraph::METIS);
    graph.writeVTK("csp_M.vtk","cspE_M.vtk");
  }
  {
    SOSGraph graph(ret,SOSGraph::NATURAL);
    graph.writeVTK("csp_N.vtk","cspE_N.vtk");
  }
  {
    SOSGraph graph(ret,SOSGraph::METIS_REVERSED);
    graph.writeVTK("csp_MR.vtk","cspE_MR.vtk");
  }
  return 0;
#undef N
}
