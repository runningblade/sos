#include "SOS/SOSProblem.h"
#include <CommonFile/Timing.h>

USE_PRJ_NAMESPACE

void comparePutBSOS(const std::string& name,SOSProblem::PolyXA poly,SOSGraph::ORDER type,SemiAlgebraicSet K,sizeType d,sizeType k)
{
  poly-=SOSProblem::PolyXA("x0");
  {
    TBEG("SparsePut"+name);
    SOSProblem prob;
    prob.outputWhenSolve(true);
    prob.solverType(SOSProblem::SOLVER_MOSEK);
    //prob.solverType(SOSProblem::SOLVER_SDPA_GMP);
    prob.useGraphOrder(type);
    prob.visualizeCSP("cspPutSOS"+name);
    prob.makePutSOS("SOS",name,poly,&K,k);
    prob.makeObjective(name,SOSProblem::PolyXA("-1*x0"));
    ASSERT(prob.solve())
    TEND();
    INFOV("SparsePut: %f!",prob.getSolutionOfPolyVar(name,0))
  }
  {
    TBEG("SparseBSOS"+name);
    SOSProblem prob;
    prob.outputWhenSolve(true);
    prob.solverType(SOSProblem::SOLVER_MOSEK);
    //prob.solverType(SOSProblem::SOLVER_SDPA_GMP);
    prob.useGraphOrder(type);
    prob.visualizeCSP("cspBSOS"+name);
    prob.makeBSOS("SOS",name,poly,K,d,k);
    prob.makeObjective(name,SOSProblem::PolyXA("-1*x0"));
    ASSERT(prob.solve())
    TEND();
    INFOV("SparseBSOS: %f!",prob.getSolutionOfPolyVar(name,0))
  }
}
void exampleChainedWood(sizeType N)
{
#define VAR(N) ("a"+std::to_string((N)-1))
  ASSERT(N%4==0)
  std::ostringstream oss;
  SemiAlgebraicSet K;
  for(sizeType i=1; i<=(sizeType)(N/2-1); i++) {
    sizeType j=2*i-1;
    if(i>1)
      oss << "+";
    oss << "100*(" << VAR(j+1) << "-" << VAR(j) << "^2)^2+(1-" << VAR(j) << ")^2+90*(" << VAR(j+3) << "-" << VAR(j+2) << "^2)^2+";
    oss << "(1-" << VAR(j+2) << ")^2+10*(" << VAR(j+1) << "+" << VAR(j+3) << "-2)^2+0.1*(" << VAR(j+1) << "-" << VAR(j+3) << ")^2";
    //push K
    std::ostringstream ossK;
    ossK << "1-" << VAR(j) << "-" << VAR(j+1) << "-" << VAR(j+2) << "-" << VAR(j+3);
    K._f.push_back(SOSProblem::PolyXA(ossK.str()));
  }
  for(sizeType j=1; j<=N; j++)
    K._f.push_back(SOSProblem::PolyXA(VAR(j)));
  //std::cout << oss.str() << std::endl;
  SOSProblem::PolyXA poly(oss.str());
  comparePutBSOS("ChainWood",poly,SOSGraph::NATURAL,K,2,2);
#undef VAR
}
void exampleGeneralizedRosenbrock(sizeType N)
{
#define VAR(N) ("a"+std::to_string((N)-1))
  std::ostringstream oss;
  SemiAlgebraicSet K;
  for(sizeType i=2; i<=N; i++) {
    if(i>2)
      oss << "+";
    oss << "100*(" << VAR(i) << "-" << VAR(i-1) << "^2)^2+(1-" << VAR(i) << ")^2";
    //push K
    std::ostringstream ossK;
    ossK << "1-" << VAR(i) << "-" << VAR(i-1);
    K._f.push_back(SOSProblem::PolyXA(ossK.str()));
  }
  //push K
  for(sizeType j=1; j<=N; j++)
    K._f.push_back(SOSProblem::PolyXA(VAR(j)));
  //std::cout << oss.str() << std::endl;
  SOSProblem::PolyXA poly(oss.str());
  comparePutBSOS("GeneralizedRosenbrock",poly,SOSGraph::NATURAL,K,2,2);
#undef VAR
}
int main()
{
  exampleChainedWood(20);
  exampleGeneralizedRosenbrock(100);
  return 0;
}
